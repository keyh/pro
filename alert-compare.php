<div class="alert alert-info alert-compare">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<div class="list-compare pull-right">
		<div class="item-compare">
			<img src="assets/img/fashion/1.png" alt="">
			<span>x</span>
		</div>
		<div class="item-compare"><img src="assets/img/fashion/3.png" alt=""><span>x</span></div>
		<div class="item-compare"><img src="assets/img/fashion/2.png" alt=""><span>x</span></div>
		<div class="item-compare"><img src="assets/img/fashion/4.png" alt=""><span>x</span></div>
		<div class="item-compare"><img src="assets/img/fashion/5.png" alt=""><span>x</span></div>
		<button class="btn btn-primary btn-compare"><i class="fa fa-check"></i> Compare</button>
	</div>
</div>