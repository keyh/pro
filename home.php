<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Responsive Carousel - jCarousel Examples</title>

       <!--  <link rel="stylesheet" type="text/css" href="assets/css/jcarousel.responsive.css"> -->
        <link rel="stylesheet" type="text/css" href="assets/css/lily-store.css">
        
        <script src="assets/vendor/jquery/dist/jquery.js" type="text/javascript" charset="utf-8" async defer></script>
        <script src="assets/js/Lily-store.js"></script>
        <script src="assets/js/main.js"></script>
        <script src="assets/js/jquery.jcarousel.min.js"></script>
    </head>
    <body>

        <div class="container">
            <div class="row category-product">
                <div class="title-category">
                    <a href="#"> <span>Thời trang</span> </a>
                </div>
                <div class="jcarousel-wrapper">
                    <div class="jcarousel">
                        <ul class="list-item-product">
                            <li>
                                <a href="#"><img src="assets/img/fashion/1.png" alt="Image 1"></a>
                                <div class="list-button">
                                   <a href="#"> <i class="fa fa-line-chart"> </i></a>
                                   <a href="#"> <i class="fa fa-search"> </i></a>
                                   <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
                                   <a href="#"> <i class="fa fa-heart"> </i></a>
                                   <a href="#"> <i class="fa fa-exchange"> </i></a>
                                </div>
                            </li>
                            <li>
                                <a href="#"><img src="assets/img/fashion/2.png" alt="Image 1"></a>
                                <div class="list-button">
                                   <a href="#"> <i class="fa fa-line-chart"> </i></a>
                                   <a href="#"> <i class="fa fa-search"> </i></a>
                                   <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
                                   <a href="#"> <i class="fa fa-heart"> </i></a>
                                   <a href="#"> <i class="fa fa-exchange"> </i></a>
                                </div>
                            </li>
                            <li>
                                <a href="#"><img src="assets/img/fashion/3.png" alt="Image 1"></a>
                                <div class="list-button">
                                   <a href="#"> <i class="fa fa-line-chart"> </i></a>
                                   <a href="#"> <i class="fa fa-search"> </i></a>
                                   <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
                                   <a href="#"> <i class="fa fa-heart"> </i></a>
                                   <a href="#"> <i class="fa fa-exchange"> </i></a>
                                </div>
                            </li>
                            <li>
                                <a href="#"><img src="assets/img/fashion/4.png" alt="Image 1"></a>
                                <div class="list-button">
                                   <a href="#"> <i class="fa fa-line-chart"> </i></a>
                                   <a href="#"> <i class="fa fa-search"> </i></a>
                                   <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
                                   <a href="#"> <i class="fa fa-heart"> </i></a>
                                   <a href="#"> <i class="fa fa-exchange"> </i></a>
                                </div>
                            </li>
                            <li>
                                <a href="#"><img src="assets/img/fashion/5.png" alt="Image 1"></a>
                                <div class="list-button">
                                   <a href="#"> <i class="fa fa-line-chart"> </i></a>
                                   <a href="#"> <i class="fa fa-search"> </i></a>
                                   <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
                                   <a href="#"> <i class="fa fa-heart"> </i></a>
                                   <a href="#"> <i class="fa fa-exchange"> </i></a>
                                </div>
                            </li>
                            <li>
                                <a href="#"><img src="assets/img/fashion/6.png" alt="Image 1"></a>
                                <div class="list-button">
                                   <a href="#"> <i class="fa fa-line-chart"> </i></a>
                                   <a href="#"> <i class="fa fa-search"> </i></a>
                                   <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
                                   <a href="#"> <i class="fa fa-heart"> </i></a>
                                   <a href="#"> <i class="fa fa-exchange"> </i></a>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <a href="#" class="jcarousel-control-prev">&lsaquo;</a>
                    <a href="#" class="jcarousel-control-next">&rsaquo;</a>
                </div>
            </div>

            <div class="row category-product">
                <div class="title-category">
                    <a href="#"> <span>Đồng hồ nam</span> </a>
                </div>
                <div class="jcarousel-wrapper">
                    <div class="jcarousel">
                        <ul class="list-item-product">
                            <li>
                                <a href="#"><img src="assets/img/fashion/c1.jpg" alt=""></a>
                                <div class="list-button">
                                   <a href="#"> <i class="fa fa-line-chart"> </i></a>
                                   <a href="#"> <i class="fa fa-search"> </i></a>
                                   <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
                                   <a href="#"> <i class="fa fa-heart"> </i></a>
                                   <a href="#"> <i class="fa fa-exchange"> </i></a>
                                </div>
                            </li>
                            <li>
                                <a href="#"><img src="assets/img/fashion/c2.jpg" alt=""></a>
                                <div class="list-button">
                                   <a href="#"> <i class="fa fa-line-chart"> </i></a>
                                   <a href="#"> <i class="fa fa-search"> </i></a>
                                   <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
                                   <a href="#"> <i class="fa fa-heart"> </i></a>
                                   <a href="#"> <i class="fa fa-exchange"> </i></a>
                                </div>
                            </li>
                            <li>
                                <a href="#"><img src="assets/img/fashion/c3.jpg" alt=""></a>
                                <div class="list-button">
                                   <a href="#"> <i class="fa fa-line-chart"> </i></a>
                                   <a href="#"> <i class="fa fa-search"> </i></a>
                                   <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
                                   <a href="#"> <i class="fa fa-heart"> </i></a>
                                   <a href="#"> <i class="fa fa-exchange"> </i></a>
                                </div>
                            </li>
                            <li>
                                <a href="#"><img src="assets/img/fashion/c4.jpg" alt=""></a>
                                <div class="list-button">
                                   <a href="#"> <i class="fa fa-line-chart"> </i></a>
                                   <a href="#"> <i class="fa fa-search"> </i></a>
                                   <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
                                   <a href="#"> <i class="fa fa-heart"> </i></a>
                                   <a href="#"> <i class="fa fa-exchange"> </i></a>
                                </div>
                            </li>
                            <li>
                                <a href="#"><img src="assets/img/fashion/c5.jpg" alt=""></a>
                                <div class="list-button">
                                   <a href="#"> <i class="fa fa-line-chart"> </i></a>
                                   <a href="#"> <i class="fa fa-search"> </i></a>
                                   <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
                                   <a href="#"> <i class="fa fa-heart"> </i></a>
                                   <a href="#"> <i class="fa fa-exchange"> </i></a>
                                </div>
                            </li>
                            <li>
                                <a href="#"><img src="assets/img/fashion/c6.jpg" alt=""></a>
                                <div class="list-button">
                                   <a href="#"> <i class="fa fa-line-chart"> </i></a>
                                   <a href="#"> <i class="fa fa-search"> </i></a>
                                   <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
                                   <a href="#"> <i class="fa fa-heart"> </i></a>
                                   <a href="#"> <i class="fa fa-exchange"> </i></a>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <a href="#" class="jcarousel-control-prev">&lsaquo;</a>
                    <a href="#" class="jcarousel-control-next">&rsaquo;</a>
                </div>
            </div>
            
            <section class=" section section-advertising clearfix">
                <div class="free-shipping pull-left text-center">
                    <img src="assets/img/car.png" alt="">
                    <p>Miễn phí vận chuyển</p>
                </div>
                <div class="custom-care pull-left text-center">
                    <img src="assets/img/help.png" alt="">
                    <p>Chăm sóc khách hàng</p>
                </div>
                <div class="party pull-left text-center">
                    <img src="assets/img/party.png" alt="">
                    <p>Quà tặng hấp dẫn</p>
                </div>
            </section>

            <div class="row category-product">
                <div class="title-category">
                    <a href="#"> <span>Giầy thời trang</span> </a>
                </div>
                <div class="jcarousel-wrapper">
                    <div class="jcarousel">
                        <ul class="list-item-product">
                            <li>
                                <a href="#"><img src="assets/img/fashion/s1.jpg" alt=""></a>
                                <div class="list-button">
                                   <a href="#"> <i class="fa fa-line-chart"> </i></a>
                                   <a href="#"> <i class="fa fa-search"> </i></a>
                                   <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
                                   <a href="#"> <i class="fa fa-heart"> </i></a>
                                   <a href="#"> <i class="fa fa-exchange"> </i></a>
                                </div>
                            </li>
                            <li>
                                <a href="#"><img src="assets/img/fashion/s2.jpg" alt=""></a>
                                <div class="list-button">
                                   <a href="#"> <i class="fa fa-line-chart"> </i></a>
                                   <a href="#"> <i class="fa fa-search"> </i></a>
                                   <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
                                   <a href="#"> <i class="fa fa-heart"> </i></a>
                                   <a href="#"> <i class="fa fa-exchange"> </i></a>
                                </div>
                            </li>
                            <li>
                                <a href="#"><img src="assets/img/fashion/s3.jpg" alt=""></a>
                                <div class="list-button">
                                   <a href="#"> <i class="fa fa-line-chart"> </i></a>
                                   <a href="#"> <i class="fa fa-search"> </i></a>
                                   <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
                                   <a href="#"> <i class="fa fa-heart"> </i></a>
                                   <a href="#"> <i class="fa fa-exchange"> </i></a>
                                </div>
                            </li>
                            <li>
                                <a href="#"><img src="assets/img/fashion/s4.jpg" alt=""></a>
                                <div class="list-button">
                                   <a href="#"> <i class="fa fa-line-chart"> </i></a>
                                   <a href="#"> <i class="fa fa-search"> </i></a>
                                   <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
                                   <a href="#"> <i class="fa fa-heart"> </i></a>
                                   <a href="#"> <i class="fa fa-exchange"> </i></a>
                                </div>
                            </li>
                            <li>
                                <a href="#"><img src="assets/img/fashion/s5.jpg" alt=""></a>
                                <div class="list-button">
                                   <a href="#"> <i class="fa fa-line-chart"> </i></a>
                                   <a href="#"> <i class="fa fa-search"> </i></a>
                                   <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
                                   <a href="#"> <i class="fa fa-heart"> </i></a>
                                   <a href="#"> <i class="fa fa-exchange"> </i></a>
                                </div>
                            </li>
                            <li>
                                <a href="#"><img src="assets/img/fashion/s6.jpg" alt=""></a>
                                <div class="list-button">
                                   <a href="#"> <i class="fa fa-line-chart"> </i></a>
                                   <a href="#"> <i class="fa fa-search"> </i></a>
                                   <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
                                   <a href="#"> <i class="fa fa-heart"> </i></a>
                                   <a href="#"> <i class="fa fa-exchange"> </i></a>
                                </div>
                            </li>
                        </ul>
                    </div>

                    <a href="#" class="jcarousel-control-prev">&lsaquo;</a>
                    <a href="#" class="jcarousel-control-next">&rsaquo;</a>
                </div>
            </div>

            <div class="row category-product list-client">
                <div id="carousel-category-client" class="carousel slide" data-ride="carousel">
                    <div class="carousel-inner">
                        <div class="item active">
                            <ul class=" row list-inline">
                                <li class="col-md-2 text-center">
                                    <img src="assets/img/fashion/b1.jpg" alt="">
                                </li>
                                <li class="col-md-2 text-center">
                                    <img src="assets/img/fashion/b2.png" alt="">
                                </li>
                                <li class="col-md-2 text-center">
                                    <img src="assets/img/fashion/b3.jpg" alt="">
                                </li>
                                <li class="col-md-2 text-center">
                                    <img src="assets/img/fashion/b4.jpg" alt="">
                                </li>
                                <li class="col-md-2 text-center">
                                    <img src="assets/img/fashion/b5.jpg" alt="">
                                </li>
                                <li class="col-md-2 text-center">
                                    <img src="assets/img/fashion/b6.jpg" alt="">
                                </li>
                                
                            </ul>
                        </div>

                         <div class="item ">
                            <ul class=" row list-inline">
                                <li class="col-md-2 text-center">
                                    <img src="assets/img/fashion/b7.jpg" alt="">
                                </li>
                                <li class="col-md-2 text-center">
                                    <img src="assets/img/fashion/b2.png" alt="">
                                </li>
                                <li class="col-md-2 text-center">
                                    <img src="assets/img/fashion/b3.jpg" alt="">
                                </li>
                                <li class="col-md-2 text-center">
                                    <img src="assets/img/fashion/b4.jpg" alt="">
                                </li>
                                <li class="col-md-2 text-center">
                                    <img src="assets/img/fashion/b5.jpg" alt="">
                                </li>
                                <li class="col-md-2 text-center">
                                    <img src="assets/img/fashion/b6.jpg" alt="">
                                </li>
                                
                            </ul>
                        </div>
                        
                    </div>
                    <a class="control-left carousel-control" href="#carousel-category-client" data-slide="prev"> <i class="fa fa-angle-left"></i></a>
                    <a class=" control-right carousel-control" href="#carousel-category-client" data-slide="next"><i class="fa fa-angle-right"></i> </a>
                </div>
            </div>
        </div>
    </body>
</html>
