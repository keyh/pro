</div>
	<footer class="footer footer-bg">
				<div class="container">
					<div class="row">
						<div class="col-md-4">
							<h3 class="title-introduce">Về chúng tôi</h3>
							<ul class="list-unstyled content-footer">
								<li><a href="#">Giới thiệu</a></li>
								<li><a href="#">Khách hàng thân thiết</a></li>
								<li><a href="#">Phiếu quà tặng</a></li>
								<li><a href="#">Hợp tác kinh doanh</a></li>
								<li><a href="#">Liên hệ</a></li>
								<li><a href="#">Bản đồ</a></li>
							</ul>
						</div>
						<div class="col-md-4">
							<h3 class="title-introduce">Hỗ trợ khách hàng</h3>
							<ul class="list-unstyled content-footer">
								<li><a href="#">Mua hàng và thanh toán</a></li>
								<li><a href="#">Chính sách giao hàng</a></li>
								<li><a href="#">Chính sách đổi trả hàng</a></li>
								<li><a href="#">Bảo mật</a></li>
								<li><a href="#">Câu hỏi thường gặp</a></li>
								<li><a href="#">Sơ đồ website</a></li>
							</ul>
						</div>
						<div class="col-md-4">
							<h3 class="title-introduce">Email đăng ký</h3>
							<form action="#" class="footer-email">
								<div class="input-group">
							    	<input type="text" class="form-control" placeholder="Email">
							      	<span class="input-group-btn">
							        	<button class="btn btn-primary" type="button">OK</button>
							      	</span>
							    </div>
							    <div class="footer-social">
									<button class="btn btn-default social-f"><i class="fa fa-facebook"></i></button>
							    	<button class="btn btn-default social-g"><i class="fa fa-google-plus"></i></button>
							    	<button class="btn btn-default social-t"><i class="fa fa-twitter"></i></button>
							    	
							    </div>
							</form>
						</div>
					</div>
					<div class="row coppy-right">
						<div class="col-md-6">
							<p>Copyright © 2015  . Designed by <a href="">Amdana.com</a></p>
						</div>
						<div class="col-md-6 text-right">
							<ul class="list-inline">
								<li><img src="assets/img/visa.png" alt=""> </li>
								<li><img src="assets/img/icon_payment_paypal.png" alt=""></li>
								<li><img src="assets/img/icon_payment_master_card.png" alt=""></li>
								<li><img src="assets/img/icon_fed_visa.png" alt=""></li>
							</ul>
						</div>
					</div>
				</div>
	</footer>
		<div class="mark-black"></div>
		
		<!-- Modal Quickview -->
		<div class="modal fade category-product " id="modal-id">
			<div class="modal-dialog modal-lg">
				<div class="modal-content ">
					<button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">
						<img src="assets/img/btn-close.png" alt="">
					</button>
					
					<div class="modal-body quickview-product">
						<div class="media">
							<div class="pull-left">
								<div id="carousel-product" class="carousel slide" data-ride="carousel">
								    
								    <div class="carousel-inner">
								        <div class="item active">
								        	<a href="#"><img src="assets/img/fashion/1.png" alt=""></a>
								        </div>
								        <div class="item">
								        	<a href="#"><img src="assets/img/fashion/2.png" alt=""></a>
								        </div>
								        <div class="item ">
								        	<a href="#"><img src="assets/img/fashion/3.png" alt=""></a>
								        </div>
								    </div>
								    <a class="control-left-qv carousel-control" href="#carousel-product" data-slide="prev"><i class="fa fa-angle-left"></i></a>
								    <a class="control-right-qv carousel-control" href="#carousel-product" data-slide="next"><i class="fa fa-angle-right"></i></a>
								</div>
							</div>
							<div class="media-body info-product">
								<h2 class="media-heading">Huma saren mazem kae</h2>
								<p>Fusce porttitor at ante eu egestas. Morbi vulputate diam at nibh imperdiet pretium. Nulla euismod, nibh nec tincidunt maximus, elit sem ornare nunc, et dictum elit dui sed justo. Donec scelerisque, erat vel pharetra luctus, nibh tortor efficitur nibh, non euismod leo diam ut risus.</p>
								
			            		<div class="star">
			            			<span class="fa fa-star"></span>
			            			<span class="fa fa-star"></span>
			            			<span class="fa fa-star"></span>
			            			<span class="fa fa-star"></span>
			            			<span class="fa fa-star-half-empty"></span>
			            		</div>
			            		<div class="price">
			            			<span class="price-old">$20</span>
			            			<span class="price-new">$15</span>
			            		</div>
			            		<div class="list-btn">
			            			<ul class="list-inline">
			            				<li>
			            					<form action="" method="POST" class="form-inline" role="form">
			            						<div class="form-group">
			            							<input type="text" class="form-control" value="1">
			            						</div>
			            					</form>
			            				<li>
			            					<a href="#" class="btn btn-primary add-to-cart"> Add to cart</a>
			            				</li>
			            				<li>
			            					<a href="#" class="btn btn-default top-product"> <i class="fa fa-line-chart"></i></a>
			            				</li>
			            				<li><a href="#" class="btn btn-default wishlist"><i class="fa fa-heart-o"></i></a></li>
			            				<li><a href="#" class="btn btn-default compare"><i class="fa fa-exchange"></i></a></li>

			            			</ul>
			            		</div>
							</div>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</body>
</html>