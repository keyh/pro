<div class="container">
    <table class="table view-cart">
        <thead>
            <tr>
                <th></th>
                <th></th>
                <th>Tên sản phẩm</th>
                <th>Giá sản phẩm</th>
                <th>Số lượng</th>
                <th>Tổng</th>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td><a href="#" class="remove-product"><i class="fa fa-times"></i></a></td>
                <td><a href="#"><img src="assets/img/fashion/1.png" alt=""></a></td>
                <td><a href="#" class="name-product">this is name fashion 1</a></td>
                <td>$40</td>
                <td><input type="number" class="quantity" value="2"> <a href="#" class="btn btn-primary"><i class="fa fa-refresh"></i></a> </td>
                <td>$80</td>
            </tr>
             <tr>
                <td><a href="#" class="remove-product"><i class="fa fa-times"></i></a></td>
                <td><a href="#"><img src="assets/img/fashion/1.png" alt=""></a></td>
                <td><a href="#" class="name-product">this is name fashion 1</a></td>
                <td>$40</td>
                <td><input type="number" class="quantity" value="2"> <a href="#" class="btn btn-primary"><i class="fa fa-refresh"></i></a></td>
                <td>$80</td>
            </tr>
            <tr>
                <td colspan="6">
                    <div class="coupon pull-left">
                        <input type="text" placeholder="Mã khuyến mãi">
                        <div class="btn btn-default btn-coupon">Apply</div>
                    </div>

                </td>
                
            </tr>
        </tbody>
    </table>
    <div class="row">
        <div class="col-md-6 col-md-offset-6">
        <h3>Tổng:</h3>
            <table class="table">
                <tbody>
                    <tr>
                        <th>Tổng phụ</th>
                        <td>$80</td>
                    </tr>
                    <tr>
                        <th>Phí vận chuyển</th>
                        <td>Miễn phí vận chuyển</td>
                    </tr>
                    <tr>
                        <th>Tổng</th>
                        <th>$80</th>
                    </tr>
                </tbody>
            </table>
            <div class="check-out pull-right">
                <a href="#" class="btn btn-primary">Thanh toán <i class="fa fa-arrow-circle-o-right"></i></a>
            </div>
        </div>
    </div>
</div>