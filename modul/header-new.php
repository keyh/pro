<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Title Page</title>

		<link rel="stylesheet" type="text/css" href="assets/css/lily-store.css">
       
        <script src="assets/js/lily-store.js"></script>

	</head>
	<body>
		<div class="main-menu clearfix fixed-menu">
			<div class="menu-left">
				<div class="menu-bar">
					<nav class="navbar navbar-default" role="navigation">
						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
							<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
								<span class="sr-only">Toggle navigation</span>
								<span class="fa fa-bars"></span>
							</button>
							<a class="navbar-brand logo" href="#"><img src="assets/img/logo-main.png" alt=""></a>
						</div>
					
						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse navbar-ex1-collapse navbar-right">
							<ul class="nav navbar-nav parent-menu">
								<li>
									<a href="#">Women</a>
									<ul class="list-unstyled child-menu">
										<li><a href="#">Shose</a></li>
										<li><a href="#">T-shirt</a></li>
										<li><a href="#">Hats</a></li>
									</ul>
								</li>
								<li><a href="#">Men</a></li>
								<li><a href="#">About Us</a></li>
								<li><a href="#">Contact</a></li>
							</ul>
							
						</div><!-- /.navbar-collapse -->
					</nav>
				</div>
			</div>
			<div class="menu-right">
				<ul class="list-inline parent-menu">
					<li class="my-cart"><a href="#"><i class="fa fa-shopping-cart"></i></a></li>
					
					<li class="my-search">
						
						<form role="search" method="get" action="#">
							<input type="search" class="form-control" autocomplete="off" placeholder="Search …" value="" name="s" title="Search for:">
						</form>
					</li>
					<li class="my-more">
						<a href="#"><i class="fa fa-ellipsis-v"></i></a>
						<ul class="list-unstyled child-menu">
							<li><a href="#">Tài khoản</a></li>
							<li><a href="#">Sản phẩm yêu thích</a></li>
							<li><a href="#">Kiểm tra giỏ hàng</a></li>
						</ul>
					</li>
					<li class="btn-up-post"><a href="#"><i class="fa fa-pencil-square-o"></i> Đăng bài</a></li>
				</ul>
			</div>
		</div>
		<div class="site-content">