<div class="container">
	<div id="tabsleft" class="tabbable tabs-left wrap-product">
		<ul class="row list-inline up-steps">
		  	<li class="col-md-3">
		  		<a href="#tabsleft-tab1" data-toggle="tab" class="step-product">1</a>
		  		<p class="description">Thông tin chung sản phẩm</p>
		  	</li>
			<li class="col-md-3">
				<a href="#tabsleft-tab2" data-toggle="tab" class="step-product">2</a>
				<p class="description">Mô tả sản phẩm</p>
			</li>
			<li class="col-md-3">
				<a href="#tabsleft-tab3" data-toggle="tab" class="step-product">3</a>
				<p class="description">Thêm ảnh mô tả</p>
			</li>
			<li class="col-md-3">
				<a href="#tabsleft-tab4" data-toggle="tab" class="step-product">4</a>
				<p class="description">Thông tin chi tiết sản phẩm</p>
			</li>
			
		</ul>
        <div id="bar" class="progress progress-info progress-striped">
          <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%;"></div>
        </div>
		<div class="tab-content">
		    <div class="tab-pane" id="tabsleft-tab1">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group row">
					      	<label for="input-name-product" class="col-lg-4 control-label">Tên sản phẩm</label>
					      	<div class="col-lg-8">
					        	<input type="text" class="form-control" id="input-name-product" placeholder="Tên sản phâm">
					      	</div>
					    </div>
					    <div class="form-group row">
					      	<label for="input-number" class="col-lg-4 control-label">Số lượng</label>
					      	<div class="col-lg-8">
					        	<input type="text" class="form-control" id="input-number" placeholder="Số lượng">
					      	</div>
					    </div>
					    <div class="form-group row">
					      	<label for="input-price" class="col-lg-4 control-label">Giá sản phẩm</label>
					      	<div class="col-lg-8">
					        	<input type="text" class="form-control" id="input-price" placeholder="Giá sản phẩm">
					      	</div>
					    </div>
					    <div class="form-group row">
					      	<label for="input-style" class="col-lg-4 control-label">Kiểu mẫu</label>
					      	<div class="col-lg-8">
					        	<input type="text" class="form-control" id="input-style" placeholder="Kiểu mẫu">
					      	</div>
					    </div>
					</div>
					<div class="col-md-6">
						<div class="form-group row">
					      	<label for="input-feature-product" class="col-lg-4 control-label">Ảnh đại diện</label>
					      	<div class="col-lg-8">
					        	<input type="file" name="fileToUpload" id="fileToUpload">
					      	</div>
					    </div>	
					    <div class="form-group row">
					    <label for="input-category-product" class="col-lg-4 control-label">Danh mục</label>
					      	<div class="col-lg-8">
								<select multiple="multiple" name="ProductCategory[id_category][]" id="ProductCategory_id_category">
									<option value=""> --- None --- </option>
									<option value="61">Gia dụng</option>
									<option value="82">&nbsp;&nbsp;&nbsp;Nhà bếp</option>
									<option value="83">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Soong</option>
									<option value="70">Điện Thoại</option>
									<option value="79">&nbsp;&nbsp;&nbsp;SOong</option>
									<option value="71">Máy tính</option>
									<option value="73">&nbsp;&nbsp;&nbsp;Dell</option>
									<option value="75">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DELL VORTRO</option>
									<option value="74">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dell INSPIRON</option>
									<option value="62">Điện Tử</option>
									<option value="80">&nbsp;&nbsp;&nbsp;Sam Sung</option>
									<option value="81">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ti vi</option>
									<option value="63">Thời Trang</option>
									<option value="78">&nbsp;&nbsp;&nbsp;Quần áo nữ</option>
									<option value="77">&nbsp;&nbsp;&nbsp;Thời Trang Nam</option>
									<option value="76">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Giày đen</option>
									<option value="65">Nhà cửa - Trang trí</option>
									<option value="84">&nbsp;&nbsp;&nbsp;Trung cư</option>
									<option value="85">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rèm cưa</option>
									<option value="67">Thê Thao</option>
									<option value="68">Điện gia dụng</option>
									<option value="69">Văn phòng</option>
								</select>
							</div>
						</div>
					</div>
				</div>		    
		    </div>
		    <div class="tab-pane" id="tabsleft-tab2">
		    	<textarea class="form-control" rows="10" id="textArea"></textarea> 
		    </div>
			<div class="tab-pane" id="tabsleft-tab3">
				<div class="row">
						<div class="col-md-6">
							<div class="form-group row">
						      	<label for="input-name-product" class="col-lg-4 control-label">Thêm ảnh mô tả</label>
						      	<div class="col-lg-8">
						        	<input type="file" name="fileToUpload" id="fileToUpload">
						      	</div>
						    </div>
						</div>
						<div class="col-md-6">
							<div class="garally-product">
								<a href="#"><img src="assets/img/fashion/1.png" alt=""></a>
								<a href="#"><img src="assets/img/fashion/2.png" alt=""></a>
								<a href="#"><img src="assets/img/fashion/3.png" alt=""></a>
								<a href="#"><img src="assets/img/fashion/4.png" alt=""></a>
								<a href="#"><img src="assets/img/fashion/4.png" alt=""></a>
								<a href="#"><img src="assets/img/fashion/3.png" alt=""></a>
								<a href="#"><img src="assets/img/fashion/4.png" alt=""></a>
								<a href="#"><img src="assets/img/fashion/4.png" alt=""></a>
								
							</div>
						</div>
				</div>
		    </div>
		    <div class="tab-pane" id="tabsleft-tab4">
				<div class="row">
					<div class="col-md-6">
						<div class="form-group row">
					      	<label for="input-name-product" class="col-lg-4 control-label">Tên sản phẩm</label>
					      	<div class="col-lg-8">
					        	<input type="text" class="form-control" id="input-name-product" placeholder="Tên sản phâm">
					      	</div>
					    </div>
					    <div class="form-group row">
					      	<label for="input-number" class="col-lg-4 control-label">Số lượng</label>
					      	<div class="col-lg-8">
					        	<input type="text" class="form-control" id="input-number" placeholder="Số lượng">
					      	</div>
					    </div>
					    <div class="form-group row">
					      	<label for="input-price" class="col-lg-4 control-label">Giá sản phẩm</label>
					      	<div class="col-lg-8">
					        	<input type="text" class="form-control" id="input-price" placeholder="Giá sản phẩm">
					      	</div>
					    </div>
					    <div class="form-group row">
					      	<label for="input-style" class="col-lg-4 control-label">Kiểu mẫu</label>
					      	<div class="col-lg-8">
					        	<input type="text" class="form-control" id="input-style" placeholder="Kiểu mẫu">
					      	</div>
					    </div>
					</div>
					<div class="col-md-6">
						<div class="form-group row">
					      	<label for="input-feature-product" class="col-lg-4 control-label">Ảnh đại diện</label>
					      	<div class="col-lg-8">
					        	<input type="file" name="fileToUpload" id="fileToUpload">
					      	</div>
					    </div>	
					    <div class="form-group row">
					    <label for="input-category-product" class="col-lg-4 control-label">Danh mục</label>
					      	<div class="col-lg-8">
								<select multiple="multiple" name="ProductCategory[id_category][]" id="ProductCategory_id_category">
									<option value=""> --- None --- </option>
									<option value="61">Gia dụng</option>
									<option value="82">&nbsp;&nbsp;&nbsp;Nhà bếp</option>
									<option value="83">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Soong</option>
									<option value="70">Điện Thoại</option>
									<option value="79">&nbsp;&nbsp;&nbsp;SOong</option>
									<option value="71">Máy tính</option>
									<option value="73">&nbsp;&nbsp;&nbsp;Dell</option>
									<option value="75">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;DELL VORTRO</option>
									<option value="74">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Dell INSPIRON</option>
									<option value="62">Điện Tử</option>
									<option value="80">&nbsp;&nbsp;&nbsp;Sam Sung</option>
									<option value="81">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Ti vi</option>
									<option value="63">Thời Trang</option>
									<option value="78">&nbsp;&nbsp;&nbsp;Quần áo nữ</option>
									<option value="77">&nbsp;&nbsp;&nbsp;Thời Trang Nam</option>
									<option value="76">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Giày đen</option>
									<option value="65">Nhà cửa - Trang trí</option>
									<option value="84">&nbsp;&nbsp;&nbsp;Trung cư</option>
									<option value="85">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Rèm cưa</option>
									<option value="67">Thê Thao</option>
									<option value="68">Điện gia dụng</option>
									<option value="69">Văn phòng</option>
								</select>
							</div>
						</div>
					</div>
				</div>		    
		    </div>
			<ul class="pager wizard">
				<li class="previous"><a class="btn btn-primary" href="javascript:;">Previous</a></li>
			  	<li class="next"><a class="btn btn-primary" href="javascript:;">Next</a></li>
				<li class="next finish" style="display:none;"><a class="btn btn-primary"  href="javascript:;">Finish</a></li>
			</ul>
		</div>
	</div>
</div>