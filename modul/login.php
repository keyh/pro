
<div class="modal modal-login fade" id="modal-id">
	<div class="modal-dialog modal-sm">
		<div class="modal-content">

            <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">
                <img src="assets/img/btn-close.png" alt="">
            </button>
            <div class="modal-header">
                <h4 class="modal-title">Đăng nhập</h4>
            </div>
            <div class="modal-body">
                    <form id="login-form" action="/account/login.html" method="post">
                    <div class="row btn-social margin-bottom-md-1 text-center">
                    
                            <button class="social">
                                <span class="fa-stack fa-lg">
                                  <i class="fa fa-circle fa-stack-2x face"></i>
                                  <i class="fa fa-facebook fa-stack-1x fa-inverse"></i>
                                </span>
                            </button>
                        
                            <button class="social">
                                <span class="fa-stack fa-lg">
                                  <i class="fa fa-circle fa-stack-2x twitter"></i>
                                  <i class="fa fa-twitter fa-stack-1x fa-inverse"></i>
                                </span>
                            </button>
                       
                            <button class="social">
                                <span class="fa-stack fa-lg">
                                  <i class="fa fa-circle fa-stack-2x google-plus"></i>
                                  <i class="fa fa-google-plus fa-stack-1x fa-inverse"></i>
                                </span>
                            </button>         
                    </div>
                    <div class="login-or margin-bottom-xs-2">
                        <p>OR</p>
                    </div>
                    <div class="input-group txt-username">
                        <input class="form-control" placeholder="E-Mail" name="LoginForm[username]" id="LoginForm_username" type="text">                    </div>
                    <div class="input-group txt-password">
                        <input class="form-control" placeholder="Mật khẩu" name="LoginForm[password]" id="LoginForm_password" type="password">                        <div class="errorMessage" id="LoginForm_password_em_" style="display:none"></div>                    </div>
                    <div class="form-group lost-password">
                        <div class="checkbox pull-left">
                            <p>
                                <input type="checkbox">Ghi nhớ 
                            </p>
                        </div>
                        <p class="pull-right"><a href="#">Quên mật khẩu?</a></p>
                    </div>
                    <div>
                        <input class="btn btn-primary btn-block" type="submit" name="yt0" value="Đăng nhập">                    </div>
                    </form>              
            </div>
            <div class="modal-footer">
                <p>Chưa có tài khoản? <i class="fa fa-hand-o-right"></i>  <a data-toggle="modal" href="#modal-register">Đăng ký</a> </p>
            </div>
        </div>
	</div>
</div>

<!-- modal register -->
<div class="modal fade" id="modal-register">
	<div class="modal-dialog">
		<div class="modal-content">
                        
            <button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">
                <img src="assets/img/btn-close.png" alt="">
            </button>
			<div class="modal-header">
				<h4 class="modal-title">Đăng ký</h4>
			</div>
			<div class="modal-body modal-register">
                <form class="form-horizontal padding-md-1" id="login-form" action="/account/register" method="post">
                    <fieldset>
                        <div class="form-group row">
                            <div class="col-md-6 margin-bottom-md-15">
                                <input class="form-control" placeholder="Họ đệm" name="RegistrationForm[firstname]" id="inputfirstname" type="text">
                            </div>
                            <div class="col-md-6 margin-bottom-md-15">
                                <input class="form-control last-child" placeholder="Tên" name="RegistrationForm[lastname]" id="inputlastname" type="text">          
                            </div>
                            <div class="col-md-6 margin-bottom-md-15">
                                <input class="form-control" placeholder="Telephone" name="RegistrationForm[telephone]" id="inputphone" type="text" maxlength="12">  
                            </div>
                            <div class="col-md-6 margin-bottom-md-15">
                                <input onchange="checkEmail(this.value)" class="form-control" placeholder="E-Mail Address" name="RegistrationForm[email]" id="inputEmail" type="text">           
                            </div>
                            <div class="col-md-6 margin-bottom-md-15">
                                <input class="form-control" placeholder="Password" name="RegistrationForm[password]" id="inputpass" type="password"> 
                            </div>
                            <div class="col-md-6 margin-bottom-md-15">
                                <input class="form-control" placeholder="Xác nhận lại mật khẩu" name="RegistrationForm[confirm]" id="inputconfirm" type="password">   
                
                            </div>

                        </div>
                       
                        <div class="form-group row">
                            <label class="col-md-3 control-label">Giới tính</label>
                            <div class="col-md-9">
                                <div class="radio pull-left margin-right-xs-1">
                                    <input id="ytRegistrationForm_gender" type="hidden" value="" name="RegistrationForm[gender]">
                                    <label>
                                        <input id="RegistrationForm_gender_0" value="1" checked="checked" type="radio" name="RegistrationForm[gender]">
                                        Nam
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input id="RegistrationForm_gender_1" value="0" type="radio" name="RegistrationForm[gender]">
                                        Nữ
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-md-3 control-label">Theo dõi</label>
                            <div class="col-md-9">
                                <div class="radio pull-left margin-right-xs-1">
                                    <input id="ytRegistrationForm_newsletter" type="hidden" value="" name="RegistrationForm[newsletter]">
                                    <label>
                                        <input id="RegistrationForm_newsletter_0" value="1" checked="checked" type="radio" name="RegistrationForm[newsletter]">
                                        Theo dõi
                                    </label>
                                </div>
                                <div class="radio">
                                    <label>
                                        <input id="RegistrationForm_newsletter_1" value="0" type="radio" name="RegistrationForm[newsletter]"> 
                                        Không theo dõi
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="checkbox margin-left-xs-1">
                            <input id="ytRegistrationForm_acknowledgement" type="hidden" value="0" name="RegistrationForm[acknowledgement]">
                            <input value="1" name="RegistrationForm[acknowledgement]" id="RegistrationForm_acknowledgement" type="checkbox">
                        	<p>Tôi đã đọc và đồng ý
                        		 <p>
                        </div>
                        
                        <input class="btn btn-primary pull-right margin-bottom-xs-2" type="submit" name="yt0" value="Create Account">   
                         
                    </fieldset>
                </form>
			</div>
		</div>
	</div>
</div>