<div class="container category-c3">
	<div class="row">	
		<div class="col-md-3 sidebar-left po-relative">
	        <h2><i class=" fa fa-bars"> Chuyên Mục</i></h2>
	        <ul class="list-unstyled main-category ">
	            <li>
	                <a href="#"><i class="fa fa-desktop"></i>Gia dụng</a>
	                <span class="fa fa-angle-right pull-right"></span>
	                <div class="child-category">
	                    <div class="col-sm-12">
	                      <div class="mega-group col-sm-4">
	                          <h4 class="mega-group-header"><span>Tennis</span></h4>
	                          <ul class="list-unstyled">
	                              <li><a href="#">Tennis</a></li>
	                              <li><a href="#">Coats &amp; Jackets</a></li>
	                              <li><a href="#">Blouses &amp; Shirts</a></li>
	                              <li><a href="#">Tops &amp; Tees</a></li>
	                              <li><a href="#">Hoodies &amp; Sweatshirts</a></li>
	                              <li><a href="#">Intimates</a></li>
	                          </ul>
	                      </div>
	                      <div class="mega-group col-sm-4">
	                          <h4 class="mega-group-header"><span>Swimming</span></h4>
	                          <ul class="list-unstyled">
	                              <li><a href="#">Dresses</a></li>
	                              <li><a href="#">Coats &amp; Jackets</a></li>
	                              <li><a href="#">Blouses &amp; Shirts</a></li>
	                              <li><a href="#">Tops &amp; Tees</a></li>
	                              <li><a href="#">Hoodies &amp; Sweatshirts</a></li>
	                              <li><a href="#">Intimates</a></li>
	                          </ul>
	                      </div>
	                      <div class="mega-group col-sm-4">
	                          <h4 class="mega-group-header"><span>Shoes</span></h4>
	                          <ul class="list-unstyled">
	                              <li><a href="#">Dresses</a></li>
	                              <li><a href="#">Coats &amp; Jackets</a></li>
	                              <li><a href="#">Blouses &amp; Shirts</a></li>
	                              <li><a href="#">Tops &amp; Tees</a></li>
	                              <li><a href="#">Hoodies &amp; Sweatshirts</a></li>
	                              <li><a href="#">Intimates</a></li>
	                          </ul>
	                      </div>
	                      
	                    </div>
	                </div>
	            </li>
	            <li><a href="#"><i class="fa fa-desktop"></i>Điện tử</a></li>
	            <li><a href="#"><i class="fa fa-desktop"></i>Thời trang</a></li>
	            <li><a href="#"><i class="fa fa-desktop"></i>Nhà cửa - Trang trí</a></li>
	            <li><a href="#"><i class="fa fa-desktop"></i>Sách</a></li>
	            <li><a href="#"><i class="fa fa-desktop"></i>Thể thao</a></li>
	            <li><a href="#"><i class="fa fa-desktop"></i>Điện gia dụng</a></li>
	            <li><a href="#"><i class="fa fa-desktop"></i>Văn phòng</a></li>
	            <li><a href="#"><i class="fa fa-desktop"></i>Điện thoại</a></li>  
	            <li><a href="#"><i class="fa fa-desktop"></i>Máy tính</a></li>  
	        </ul>
	  	</div>
	  	<div class="col-md-9">
	  		<div id="carousel-id" class="carousel slide" data-ride="carousel">
				    <ol class="carousel-indicators">
				        <li data-target="#carousel-id" data-slide-to="0" class=""></li>
				        <li data-target="#carousel-id" data-slide-to="1" class=""></li>
				        <li data-target="#carousel-id" data-slide-to="2" class="active"></li>
				    </ol>
				    <div class="carousel-inner">
				        <div class="item">
				            <img data-src="holder.js/900x500/auto/#777:#7a7a7a/text:First slide" alt="First slide" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI5MDAiIGhlaWdodD0iNTAwIj48cmVjdCB3aWR0aD0iOTAwIiBoZWlnaHQ9IjUwMCIgZmlsbD0iIzc3NyI+PC9yZWN0Pjx0ZXh0IHRleHQtYW5jaG9yPSJtaWRkbGUiIHg9IjQ1MCIgeT0iMjUwIiBzdHlsZT0iZmlsbDojN2E3YTdhO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1zaXplOjU2cHg7Zm9udC1mYW1pbHk6QXJpYWwsSGVsdmV0aWNhLHNhbnMtc2VyaWY7ZG9taW5hbnQtYmFzZWxpbmU6Y2VudHJhbCI+Rmlyc3Qgc2xpZGU8L3RleHQ+PC9zdmc+">
				            <div class="container">
				                <div class="carousel-caption">
				                    <h1>Example headline.</h1>
				                    <p>Note: If you're viewing this page via a <code>file://</code> URL, the "next" and "previous" Glyphicon buttons on the left and right might not load/display properly due to web browser security rules.</p>
				                    <p><a class="btn btn-lg btn-primary" href="#" role="button">Sign up today</a></p>
				                </div>
				            </div>
				        </div>
				        <div class="item">
				            <img data-src="holder.js/900x500/auto/#666:#6a6a6a/text:Second slide" alt="Second slide" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI5MDAiIGhlaWdodD0iNTAwIj48cmVjdCB3aWR0aD0iOTAwIiBoZWlnaHQ9IjUwMCIgZmlsbD0iIzY2NiI+PC9yZWN0Pjx0ZXh0IHRleHQtYW5jaG9yPSJtaWRkbGUiIHg9IjQ1MCIgeT0iMjUwIiBzdHlsZT0iZmlsbDojNmE2YTZhO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1zaXplOjU2cHg7Zm9udC1mYW1pbHk6QXJpYWwsSGVsdmV0aWNhLHNhbnMtc2VyaWY7ZG9taW5hbnQtYmFzZWxpbmU6Y2VudHJhbCI+U2Vjb25kIHNsaWRlPC90ZXh0Pjwvc3ZnPg==">
				            <div class="container">
				                <div class="carousel-caption">
				                    <h1>Another example headline.</h1>
				                    <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
				                    <p><a class="btn btn-lg btn-primary" href="#" role="button">Learn more</a></p>
				                </div>
				            </div>
				        </div>
				        <div class="item active">
				            <img data-src="holder.js/900x500/auto/#555:#5a5a5a/text:Third slide" alt="Third slide" src="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHdpZHRoPSI5MDAiIGhlaWdodD0iNTAwIj48cmVjdCB3aWR0aD0iOTAwIiBoZWlnaHQ9IjUwMCIgZmlsbD0iIzU1NSI+PC9yZWN0Pjx0ZXh0IHRleHQtYW5jaG9yPSJtaWRkbGUiIHg9IjQ1MCIgeT0iMjUwIiBzdHlsZT0iZmlsbDojNWE1YTVhO2ZvbnQtd2VpZ2h0OmJvbGQ7Zm9udC1zaXplOjU2cHg7Zm9udC1mYW1pbHk6QXJpYWwsSGVsdmV0aWNhLHNhbnMtc2VyaWY7ZG9taW5hbnQtYmFzZWxpbmU6Y2VudHJhbCI+VGhpcmQgc2xpZGU8L3RleHQ+PC9zdmc+">
				            <div class="container">
				                <div class="carousel-caption">
				                    <h1>One more for good measure.</h1>
				                    <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
				                    <p><a class="btn btn-lg btn-primary" href="#" role="button">Browse gallery</a></p>
				                </div>
				            </div>
				        </div>
				    </div>
				    <a class="left carousel-control" href="#carousel-id" data-slide="prev"><span class="glyphicon glyphicon-chevron-left"></span></a>
				    <a class="right carousel-control" href="#carousel-id" data-slide="next"><span class="glyphicon glyphicon-chevron-right"></span></a>
			</div>
	  	</div>
	</div>
	<div class="row">
		<div class="category-c3-right category-c2">
			<div class="list-item-product">
				<div class="col-md-12">
					<h3 class="title-feature pull-left">Feature Product</h3>
					<p class="view-all pull-right"><a href="#">View All</a></p>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 item-product">
						<div class="thumbnail">
							<a href="#"><img src="assets/img/fashion/1.png" alt=""></a>
							<div class="list-button">
	                             <a href="#"> <i class="fa fa-line-chart"> </i></a>
	                                <a data-toggle="modal" href='#modal-quickview'> <i class="fa fa-search"> </i></a>
	                             <a data-toggle="modal" href='#modal-addtocart'> <i class="fa fa-shopping-cart"> </i></a>
	                             <a href="#"> <i class="fa fa-heart"> </i></a>
	                             <a href="#"> <i class="fa fa-exchange"> </i></a>
	                        </div>
							<div class="caption">
								<h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
	                          	<div class="star text-center">
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star-half-empty"></span>
	                          	</div>
		                        <div class="price text-center">
		                            <div class="price-new">$50</div>
		                            <div class="price-old">$54</div>
		                        </div>
							</div>
						</div>
					</div>
					
					<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 item-product">
						<div class="thumbnail">
							<a href="#"><img src="assets/img/fashion/1.png" alt=""></a>
							<div class="list-button">
	                             <a href="#"> <i class="fa fa-line-chart"> </i></a>
	                                <a data-toggle="modal" href='#modal-quickview'> <i class="fa fa-search"> </i></a>
	                             <a data-toggle="modal" href='#modal-addtocart'> <i class="fa fa-shopping-cart"> </i></a>
	                             <a href="#"> <i class="fa fa-heart"> </i></a>
	                             <a href="#"> <i class="fa fa-exchange"> </i></a>
	                        </div>
							<div class="caption">
								<h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
	                          	<div class="star text-center">
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star-half-empty"></span>
	                          	</div>
		                        <div class="price text-center">
		                            <div class="price-new">$50</div>
		                            <div class="price-old">$54</div>
		                        </div>
							</div>
						</div>
					</div>

					<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 item-product">
						<div class="thumbnail">
							<a href="#"><img src="assets/img/fashion/1.png" alt=""></a>
							<div class="list-button">
	                             <a href="#"> <i class="fa fa-line-chart"> </i></a>
	                                <a data-toggle="modal" href='#modal-quickview'> <i class="fa fa-search"> </i></a>
	                             <a data-toggle="modal" href='#modal-addtocart'> <i class="fa fa-shopping-cart"> </i></a>
	                             <a href="#"> <i class="fa fa-heart"> </i></a>
	                             <a href="#"> <i class="fa fa-exchange"> </i></a>
	                        </div>
							<div class="caption">
								<h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
	                          	<div class="star text-center">
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star-half-empty"></span>
	                          	</div>
		                        <div class="price text-center">
		                            <div class="price-new">$50</div>
		                            <div class="price-old">$54</div>
		                        </div>
							</div>
						</div>
					</div>

					<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 item-product">
						<div class="thumbnail">
							<a href="#"><img src="assets/img/fashion/1.png" alt=""></a>
							<div class="list-button">
	                             <a href="#"> <i class="fa fa-line-chart"> </i></a>
	                                <a data-toggle="modal" href='#modal-quickview'> <i class="fa fa-search"> </i></a>
	                             <a data-toggle="modal" href='#modal-addtocart'> <i class="fa fa-shopping-cart"> </i></a>
	                             <a href="#"> <i class="fa fa-heart"> </i></a>
	                             <a href="#"> <i class="fa fa-exchange"> </i></a>
	                        </div>
							<div class="caption">
								<h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
	                          	<div class="star text-center">
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star-half-empty"></span>
	                          	</div>
		                        <div class="price text-center">
		                            <div class="price-new">$50</div>
		                            <div class="price-old">$54</div>
		                        </div>
							</div>
						</div>
					</div>	
				</div>
			</div>

			<div class="list-item-product">
				<div class="col-md-12">
					<h3 class="title-feature pull-left">Popular Product</h3>
					<p class="view-all pull-right"><a href="#">View All</a></p>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 item-product">
						<div class="thumbnail">
							<a href="#"><img src="assets/img/fashion/1.png" alt=""></a>
							<div class="list-button">
		                         <a href="#"> <i class="fa fa-line-chart"> </i></a>
		                            <a data-toggle="modal" href='#modal-quickview'> <i class="fa fa-search"> </i></a>
		                         <a data-toggle="modal" href='#modal-addtocart'> <i class="fa fa-shopping-cart"> </i></a>
		                         <a href="#"> <i class="fa fa-heart"> </i></a>
		                         <a href="#"> <i class="fa fa-exchange"> </i></a>
		                    </div>
							<div class="caption">
								<h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
		                      	<div class="star text-center">
		                        	<span class="fa fa-star"></span>
		                        	<span class="fa fa-star"></span>
		                        	<span class="fa fa-star"></span>
		                        	<span class="fa fa-star"></span>
		                        	<span class="fa fa-star-half-empty"></span>
		                      	</div>
		                        <div class="price text-center">
		                            <div class="price-new">$50</div>
		                            <div class="price-old">$54</div>
		                        </div>
							</div>
						</div>
					</div>
					
					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 item-product">
						<div class="thumbnail">
							<a href="#"><img src="assets/img/fashion/1.png" alt=""></a>
							<div class="list-button">
		                         <a href="#"> <i class="fa fa-line-chart"> </i></a>
		                            <a data-toggle="modal" href='#modal-quickview'> <i class="fa fa-search"> </i></a>
		                         <a data-toggle="modal" href='#modal-addtocart'> <i class="fa fa-shopping-cart"> </i></a>
		                         <a href="#"> <i class="fa fa-heart"> </i></a>
		                         <a href="#"> <i class="fa fa-exchange"> </i></a>
		                    </div>
							<div class="caption">
								<h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
		                      	<div class="star text-center">
		                        	<span class="fa fa-star"></span>
		                        	<span class="fa fa-star"></span>
		                        	<span class="fa fa-star"></span>
		                        	<span class="fa fa-star"></span>
		                        	<span class="fa fa-star-half-empty"></span>
		                      	</div>
		                        <div class="price text-center">
		                            <div class="price-new">$50</div>
		                            <div class="price-old">$54</div>
		                        </div>
							</div>
						</div>
					</div>

					<div class="col-xs-12 col-sm-4 col-md-4 col-lg-4 item-product">
						<div class="thumbnail">
							<a href="#"><img src="assets/img/fashion/1.png" alt=""></a>
							<div class="list-button">
		                         <a href="#"> <i class="fa fa-line-chart"> </i></a>
		                            <a data-toggle="modal" href='#modal-quickview'> <i class="fa fa-search"> </i></a>
		                         <a data-toggle="modal" href='#modal-addtocart'> <i class="fa fa-shopping-cart"> </i></a>
		                         <a href="#"> <i class="fa fa-heart"> </i></a>
		                         <a href="#"> <i class="fa fa-exchange"> </i></a>
		                    </div>
							<div class="caption">
								<h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
		                      	<div class="star text-center">
		                        	<span class="fa fa-star"></span>
		                        	<span class="fa fa-star"></span>
		                        	<span class="fa fa-star"></span>
		                        	<span class="fa fa-star"></span>
		                        	<span class="fa fa-star-half-empty"></span>
		                      	</div>
		                        <div class="price text-center">
		                            <div class="price-new">$50</div>
		                            <div class="price-old">$54</div>
		                        </div>
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 item-product">
						<div class="thumbnail">
							<a href="#"><img src="assets/img/fashion/1.png" alt=""></a>
							<div class="list-button">
		                         <a href="#"> <i class="fa fa-line-chart"> </i></a>
		                            <a data-toggle="modal" href='#modal-quickview'> <i class="fa fa-search"> </i></a>
		                         <a data-toggle="modal" href='#modal-addtocart'> <i class="fa fa-shopping-cart"> </i></a>
		                         <a href="#"> <i class="fa fa-heart"> </i></a>
		                         <a href="#"> <i class="fa fa-exchange"> </i></a>
		                    </div>
							<div class="caption">
								<h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
		                      	<div class="star text-center">
		                        	<span class="fa fa-star"></span>
		                        	<span class="fa fa-star"></span>
		                        	<span class="fa fa-star"></span>
		                        	<span class="fa fa-star"></span>
		                        	<span class="fa fa-star-half-empty"></span>
		                      	</div>
		                        <div class="price text-center">
		                            <div class="price-new">$50</div>
		                            <div class="price-old">$54</div>
		                        </div>
							</div>
						</div>
					</div>
					
					<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 item-product">
						<div class="thumbnail">
							<a href="#"><img src="assets/img/fashion/1.png" alt=""></a>
							<div class="list-button">
		                         <a href="#"> <i class="fa fa-line-chart"> </i></a>
		                            <a data-toggle="modal" href='#modal-quickview'> <i class="fa fa-search"> </i></a>
		                         <a data-toggle="modal" href='#modal-addtocart'> <i class="fa fa-shopping-cart"> </i></a>
		                         <a href="#"> <i class="fa fa-heart"> </i></a>
		                         <a href="#"> <i class="fa fa-exchange"> </i></a>
		                    </div>
							<div class="caption">
								<h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
		                      	<div class="star text-center">
		                        	<span class="fa fa-star"></span>
		                        	<span class="fa fa-star"></span>
		                        	<span class="fa fa-star"></span>
		                        	<span class="fa fa-star"></span>
		                        	<span class="fa fa-star-half-empty"></span>
		                      	</div>
		                        <div class="price text-center">
		                            <div class="price-new">$50</div>
		                            <div class="price-old">$54</div>
		                        </div>
							</div>
						</div>
					</div>

					<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 item-product">
						<div class="thumbnail">
							<a href="#"><img src="assets/img/fashion/1.png" alt=""></a>
							<div class="list-button">
		                         <a href="#"> <i class="fa fa-line-chart"> </i></a>
		                            <a data-toggle="modal" href='#modal-quickview'> <i class="fa fa-search"> </i></a>
		                         <a data-toggle="modal" href='#modal-addtocart'> <i class="fa fa-shopping-cart"> </i></a>
		                         <a href="#"> <i class="fa fa-heart"> </i></a>
		                         <a href="#"> <i class="fa fa-exchange"> </i></a>
		                    </div>
							<div class="caption">
								<h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
		                      	<div class="star text-center">
		                        	<span class="fa fa-star"></span>
		                        	<span class="fa fa-star"></span>
		                        	<span class="fa fa-star"></span>
		                        	<span class="fa fa-star"></span>
		                        	<span class="fa fa-star-half-empty"></span>
		                      	</div>
		                        <div class="price text-center">
		                            <div class="price-new">$50</div>
		                            <div class="price-old">$54</div>
		                        </div>
							</div>
						</div>
					</div>

					<div class="col-xs-12 col-sm-4 col-md-3 col-lg-3 item-product">
						<div class="thumbnail">
							<a href="#"><img src="assets/img/fashion/1.png" alt=""></a>
							<div class="list-button">
	                             <a href="#"> <i class="fa fa-line-chart"> </i></a>
	                                <a data-toggle="modal" href='#modal-quickview'> <i class="fa fa-search"> </i></a>
	                             <a data-toggle="modal" href='#modal-addtocart'> <i class="fa fa-shopping-cart"> </i></a>
	                             <a href="#"> <i class="fa fa-heart"> </i></a>
	                             <a href="#"> <i class="fa fa-exchange"> </i></a>
	                        </div>
							<div class="caption">
								<h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
	                          	<div class="star text-center">
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star-half-empty"></span>
	                          	</div>
		                        <div class="price text-center">
		                            <div class="price-new">$50</div>
		                            <div class="price-old">$54</div>
		                        </div>
							</div>
						</div>
					</div>
				</div>
			</div>			
		</div>
	</div>
</div>