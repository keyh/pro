
<div class="container archive">
   	<div class="portfolio-content">	
   		<div class="list-category">    
			<div class="portfolio-categ filter list-inline">
		      	<div class="all category_item active">
		      		<img src="assets/img/iconcategory/noibat_select.png" alt="">
		      		<a href="#">Nổi bật</a>
		      		
		      	</div>
		      	<div class="cat-item-1 category_item">
		      		<img src="assets/img/iconcategory/giadinh_select.png" alt="">
		      		<a href="#">Gia đình</a>
		      		<ul class="list-unstyled category_child">
		      			<li><a href="#">Đồ gia dụng, sinh hoạt</a></li>
		      			<li><a href="#">Đồ dùng cho trẻ em</a></li>
		      			<li><a href="#">Vật nuôi thú cưng</a></li>
		      			<li><a href="#">Đồ dùng trong bếp</a></li>
		      			<li><a href="#">Đồ dùng cho phòng ngủ</a></li>
		      		</ul>
		      	</div>
		    	<div class="cat-item-2 category_item">
		    		<img src="assets/img/iconcategory/dienmay_select.png" alt="">
		    		<a href="#"> Điện máy</a>
		    		<ul class="list-unstyled category_child">
		      			<li><a href="#">Đồ gia dụng, sinh hoạt</a></li>
		      			<li><a href="#">Đồ dùng cho trẻ em</a></li>
		      			<li><a href="#">Vật nuôi thú cưng</a></li>
		      			<li><a href="#">Đồ dùng trong bếp</a></li>
		      			<li><a href="#">Đồ dùng cho phòng ngủ</a></li>
		      		</ul>
		    	</div>
		    	<div class="cat-item-3 category_item ">
		    		<img src="assets/img/iconcategory/lamdep_select.png" alt="">
		    		<a href="#">Làm đẹp</a>
		    	</div>
		    	<div class="cat-item-4 category_item ">
		    		<img src="assets/img/iconcategory/dichvu_select.png" alt="">
		    		<a href="#"> Dịch vụ</a>
		    	</div>
		    	<div class="cat-item-5 category_item ">
		    		<img src="assets/img/iconcategory/noithat_select.png" alt="">
		    		<a href="#"> Nội thất</a>
		    		<ul class="list-unstyled category_child">
		      			<li><a href="#">Đồ gia dụng, sinh hoạt</a></li>
		      			<li><a href="#">Đồ dùng cho trẻ em</a></li>
		      			<li><a href="#">Vật nuôi thú cưng</a></li>
		      			<li><a href="#">Đồ dùng trong bếp</a></li>
		      			<li><a href="#">Đồ dùng cho phòng ngủ</a></li>
		      		</ul>
		    	</div>
		    	<div class="cat-item-6 category_item ">
		    		<img src="assets/img/iconcategory/thietbi_select.png" alt="">
		    		<a href="#">Thiết bị</a>
		    	</div>
		    	<div class="cat-item-8 category_item ">
		    		<img src="assets/img/iconcategory/khac_select.png" alt="">
		    		<a href="#">Khác</a>
		    		<ul class="list-unstyled category_child">
		      			<li><a href="#">Đồ gia dụng, sinh hoạt</a></li>
		      			<li><a href="#">Đồ dùng cho trẻ em</a></li>
		      			<li><a href="#">Vật nuôi thú cưng</a></li>
		      			<li><a href="#">Đồ dùng trong bếp</a></li>
		      			<li><a href="#">Đồ dùng cho phòng ngủ</a></li>
		      		</ul>
		    	</div>
		    	<div class="clearfix"></div>
	    	</div>
	    </div>  
	    <ul class="portfolio-area list-item-product ">	
	 		<li class="portfolio-item2" data-id="id-0" data-type="cat-item-4">	
				<a href="#"><img src="assets/img/fashion/4.png" width="284" height="285" alt="Image 1"></a>
		       	<div class="list-button">
		            <a href="#"> <i class="fa fa-line-chart"> </i></a>
		            <a href="#"> <i class="fa fa-search"> </i></a>
		            <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
		            <a href="#"> <i class="fa fa-heart"> </i></a>
		            <a href="#"> <i class="fa fa-exchange"> </i></a>
		        </div>
			    <h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
			    <div class="star text-center">
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star-half-empty"></span>
			    </div>
		        <div class="price text-center">
		            <div class="price-new">$50</div>
		            <div class="price-old">$54</div>
		        </div>
	        </li>
	 		<li class="portfolio-item2" data-id="id-1" data-type="cat-item-1">	
				<a href="#"><img src="assets/img/fashion/1.png" width="284" height="285" alt="Image 1"></a>
		       	<div class="list-button">
		            <a href="#"> <i class="fa fa-line-chart"> </i></a>
		            <a href="#"> <i class="fa fa-search"> </i></a>
		            <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
		            <a href="#"> <i class="fa fa-heart"> </i></a>
		            <a href="#"> <i class="fa fa-exchange"> </i></a>
		        </div>
			    <h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
			    <div class="star text-center">
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star-half-empty"></span>
			    </div>
		        <div class="price text-center">
		            <div class="price-new">$50</div>
		            <div class="price-old">$54</div>
		        </div>
	        </li>
	 		<li class="portfolio-item2" data-id="id-2" data-type="cat-item-3">	
				<a href="#"><img src="assets/img/fashion/3.png" width="284" height="285" alt="Image 1"></a>
		       	<div class="list-button">
		            <a href="#"> <i class="fa fa-line-chart"> </i></a>
		            <a href="#"> <i class="fa fa-search"> </i></a>
		            <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
		            <a href="#"> <i class="fa fa-heart"> </i></a>
		            <a href="#"> <i class="fa fa-exchange"> </i></a>
		        </div>
			    <h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
			    <div class="star text-center">
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star-half-empty"></span>
			    </div>
		        <div class="price text-center">
		            <div class="price-new">$50</div>
		            <div class="price-old">$54</div>
		        </div>
	        </li>
	        <li class="portfolio-item2" data-id="id-3" data-type="cat-item-1">	
				<a href="#"><img src="assets/img/fashion/6.png" width="284" height="285" alt="Image 1"></a>
		       	<div class="list-button">
		            <a href="#"> <i class="fa fa-line-chart"> </i></a>
		            <a href="#"> <i class="fa fa-search"> </i></a>
		            <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
		            <a href="#"> <i class="fa fa-heart"> </i></a>
		            <a href="#"> <i class="fa fa-exchange"> </i></a>
		        </div>
			    <h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
			    <div class="star text-center">
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star-half-empty"></span>
			    </div>
		        <div class="price text-center">
		            <div class="price-new">$50</div>
		            <div class="price-old">$54</div>
		        </div>
	        </li>
	        <li class="portfolio-item2" data-id="id-4" data-type="cat-item-4">	
				<a href="#"><img src="assets/img/fashion/7.png" width="284" height="285" alt="Image 1"></a>
		       	<div class="list-button">
		            <a href="#"> <i class="fa fa-line-chart"> </i></a>
		            <a href="#"> <i class="fa fa-search"> </i></a>
		            <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
		            <a href="#"> <i class="fa fa-heart"> </i></a>
		            <a href="#"> <i class="fa fa-exchange"> </i></a>
		        </div>
			    <h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
			    <div class="star text-center">
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star-half-empty"></span>
			    </div>
		        <div class="price text-center">
		            <div class="price-new">$50</div>
		            <div class="price-old">$54</div>
		        </div>
	        </li>
	        <li class="portfolio-item2" data-id="id-5" data-type="cat-item-4">	
				<a href="#"><img src="assets/img/fashion/2.png" width="284" height="285" alt="Image 1"></a>
		       	<div class="list-button">
		            <a href="#"> <i class="fa fa-line-chart"> </i></a>
		            <a href="#"> <i class="fa fa-search"> </i></a>
		            <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
		            <a href="#"> <i class="fa fa-heart"> </i></a>
		            <a href="#"> <i class="fa fa-exchange"> </i></a>
		        </div>
			    <h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
			    <div class="star text-center">
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star-half-empty"></span>
			    </div>
		        <div class="price text-center">
		            <div class="price-new">$50</div>
		            <div class="price-old">$54</div>
		        </div>
	        </li>
	        <li class="portfolio-item2" data-id="id-6" data-type="cat-item-1">	
				<a href="#"><img src="assets/img/fashion/1.png" width="284" height="285" alt="Image 1"></a>
		       	<div class="list-button">
		            <a href="#"> <i class="fa fa-line-chart"> </i></a>
		            <a href="#"> <i class="fa fa-search"> </i></a>
		            <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
		            <a href="#"> <i class="fa fa-heart"> </i></a>
		            <a href="#"> <i class="fa fa-exchange"> </i></a>
		        </div>
			    <h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
			    <div class="star text-center">
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star-half-empty"></span>
			    </div>
		        <div class="price text-center">
		            <div class="price-new">$50</div>
		            <div class="price-old">$54</div>
		        </div>
	        </li>
	        <li class="portfolio-item2" data-id="id-7" data-type="cat-item-3">	
				<a href="#"><img src="assets/img/fashion/1.png" width="284" height="285" alt="Image 1"></a>
		       	<div class="list-button">
		            <a href="#"> <i class="fa fa-line-chart"> </i></a>
		            <a href="#"> <i class="fa fa-search"> </i></a>
		            <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
		            <a href="#"> <i class="fa fa-heart"> </i></a>
		            <a href="#"> <i class="fa fa-exchange"> </i></a>
		        </div>
			    <h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
			    <div class="star text-center">
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star-half-empty"></span>
			    </div>
		        <div class="price text-center">
		            <div class="price-new">$50</div>
		            <div class="price-old">$54</div>
		        </div>
	        </li>
	        <li class="portfolio-item2" data-id="id-8" data-type="cat-item-3">	
				<a href="#"><img src="assets/img/fashion/5.png" width="284" height="285" alt="Image 1"></a>
		       	<div class="list-button">
		            <a href="#"> <i class="fa fa-line-chart"> </i></a>
		            <a href="#"> <i class="fa fa-search"> </i></a>
		            <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
		            <a href="#"> <i class="fa fa-heart"> </i></a>
		            <a href="#"> <i class="fa fa-exchange"> </i></a>
		        </div>
			    <h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
			    <div class="star text-center">
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star-half-empty"></span>
			    </div>
		        <div class="price text-center">
		            <div class="price-new">$50</div>
		            <div class="price-old">$54</div>
		        </div>
	        </li>
	        <li class="portfolio-item2" data-id="id-9" data-type="cat-item-2">	
				<a href="#"><img src="assets/img/fashion/4.png" width="284" height="285" alt="Image 1"></a>
		       	<div class="list-button">
		            <a href="#"> <i class="fa fa-line-chart"> </i></a>
		            <a href="#"> <i class="fa fa-search"> </i></a>
		            <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
		            <a href="#"> <i class="fa fa-heart"> </i></a>
		            <a href="#"> <i class="fa fa-exchange"> </i></a>
		        </div>
			    <h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
			    <div class="star text-center">
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star-half-empty"></span>
			    </div>
		        <div class="price text-center">
		            <div class="price-new">$50</div>
		            <div class="price-old">$54</div>
		        </div>
	        </li>
	        <li class="portfolio-item2" data-id="id-10" data-type="cat-item-2">	
				<a href="#"><img src="assets/img/fashion/7.png" width="284" height="285" alt="Image 1"></a>
		       	<div class="list-button">
		            <a href="#"> <i class="fa fa-line-chart"> </i></a>
		            <a href="#"> <i class="fa fa-search"> </i></a>
		            <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
		            <a href="#"> <i class="fa fa-heart"> </i></a>
		            <a href="#"> <i class="fa fa-exchange"> </i></a>
		        </div>
			    <h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
			    <div class="star text-center">
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star-half-empty"></span>
			    </div>
		        <div class="price text-center">
		            <div class="price-new">$50</div>
		            <div class="price-old">$54</div>
		        </div>
	        </li>
	        <li class="portfolio-item2" data-id="id-11" data-type="cat-item-2">	
				<a href="#"><img src="assets/img/fashion/1.png" width="284" height="285" alt="Image 1"></a>
		       	<div class="list-button">
		            <a href="#"> <i class="fa fa-line-chart"> </i></a>
		            <a href="#"> <i class="fa fa-search"> </i></a>
		            <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
		            <a href="#"> <i class="fa fa-heart"> </i></a>
		            <a href="#"> <i class="fa fa-exchange"> </i></a>
		        </div>
			    <h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
			    <div class="star text-center">
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star"></span>
		            <span class="fa fa-star-half-empty"></span>
			    </div>
		        <div class="price text-center">
		            <div class="price-new">$50</div>
		            <div class="price-old">$54</div>
		        </div>
	        </li>
	    </ul><!--end portfolio-area -->
	</div><!--end portfolio-content -->
</div><!-- end wrapper -->  
