<script>
	

</script>
<div class="all-category">
	<div class="category-feature-product po-relative">
		<div id="carousel-feature" class="carousel slide" data-ride="carousel">
		    <ol class="carousel-indicators">
		        <li data-target="#carousel-feature" data-slide-to="0" class=""></li>
		        <li data-target="#carousel-feature" data-slide-to="1" class=""></li>
		        <li data-target="#carousel-feature" data-slide-to="2" class="active"></li>
		    </ol>
		    <div class="carousel-inner">
		        <div class="item">
		        	<img src="assets/img/banner/banner-cat-1.jpg" alt="">
		            <div class="container">
		                <div class="carousel-caption">
		                    <h1>Example headline.</h1>
		                    <p>Note: If you're viewing this page via a <code>file://</code> URL, the "next" and "previous" Glyphicon buttons on the left and right might not load/display properly due to web browser security rules.</p>
		                </div>
		            </div>
		        </div>
		        <div class="item">
		        	<img src="assets/img/banner/banner-cat-2.jpg" alt="">
		            <div class="container">
		                <div class="carousel-caption">
		                    <h1>Another example headline.</h1>
		                    <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
		                </div>
		            </div>
		        </div>
		        <div class="item active">
		        	<img src="assets/img/banner/banner-cat-3.jpg" alt="">
		            <div class="container">
		                <div class="carousel-caption">
		                    <h1>One more for good measure.</h1>
		                    <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
		                </div>
		            </div>
		        </div>
		    </div>
		   	<a class="left carousel-control" href="#carousel-feature" data-slide="prev"> <i class="fa fa-long-arrow-left"></i></a>
		   	<a class="right carousel-control" href="#carousel-feature" data-slide="next"> <i class="fa fa-long-arrow-right"></i> </a>
		</div>
		<div class="show-cat">
			<a href="javascript:void(0)"><i class="fa fa-list-ul"></i></a>
		</div>
	</div>
	<div class="container po-relative">
		<div class="menu-cat">
			<div class="sidebar-left po-relative">
				<h2>
				<i class=" fa fa-bars"> Chuyên Mục</i>
				<i class="fa fa-times pull-right close-cat"></i>
				</h2>
				
		        <ul class="list-unstyled main-category ">
		            <li>
		                <a href="#"><i class="fa fa-desktop"></i>Gia dụng</a>
		                <span class="fa fa-angle-right pull-right"></span>
		                <div class="child-category">
		                    <div class="col-sm-12">
		                      <div class="mega-group col-sm-4">
		                          <h4 class="mega-group-header"><span>Tennis</span></h4>
		                          <ul class="list-unstyled">
		                              <li><a href="#">Tennis</a></li>
		                              <li><a href="#">Coats &amp; Jackets</a></li>
		                              <li><a href="#">Blouses &amp; Shirts</a></li>
		                              <li><a href="#">Tops &amp; Tees</a></li>
		                              <li><a href="#">Hoodies &amp; Sweatshirts</a></li>
		                              <li><a href="#">Intimates</a></li>
		                          </ul>
		                      </div>
		                      <div class="mega-group col-sm-4">
		                          <h4 class="mega-group-header"><span>Swimming</span></h4>
		                          <ul class="list-unstyled">
		                              <li><a href="#">Dresses</a></li>
		                              <li><a href="#">Coats &amp; Jackets</a></li>
		                              <li><a href="#">Blouses &amp; Shirts</a></li>
		                              <li><a href="#">Tops &amp; Tees</a></li>
		                              <li><a href="#">Hoodies &amp; Sweatshirts</a></li>
		                              <li><a href="#">Intimates</a></li>
		                          </ul>
		                      </div>
		                      <div class="mega-group col-sm-4">
		                          <h4 class="mega-group-header"><span>Shoes</span></h4>
		                          <ul class="list-unstyled">
		                              <li><a href="#">Dresses</a></li>
		                              <li><a href="#">Coats &amp; Jackets</a></li>
		                              <li><a href="#">Blouses &amp; Shirts</a></li>
		                              <li><a href="#">Tops &amp; Tees</a></li>
		                              <li><a href="#">Hoodies &amp; Sweatshirts</a></li>
		                              <li><a href="#">Intimates</a></li>
		                          </ul>
		                      </div>
		                      
		                    </div>
		                </div>
		            </li>
		            <li><a href="#"><i class="fa fa-desktop"></i>Điện tử</a></li>
		            <li><a href="#"><i class="fa fa-desktop"></i>Thời trang</a></li>
		            <li><a href="#"><i class="fa fa-desktop"></i>Nhà cửa - Trang trí</a></li>
		            <li><a href="#"><i class="fa fa-desktop"></i>Sách</a></li>
		            <li><a href="#"><i class="fa fa-desktop"></i>Thể thao</a></li>
		            <li><a href="#"><i class="fa fa-desktop"></i>Điện gia dụng</a></li>
		            <li><a href="#"><i class="fa fa-desktop"></i>Văn phòng</a></li>
		            <li><a href="#"><i class="fa fa-desktop"></i>Điện thoại</a></li>  
		            <li><a href="#"><i class="fa fa-desktop"></i>Máy tính</a></li>  
		        </ul>
	        </div>
		</div>

    	<div class="show-all-product-category">
			<div id="posts" class="row list-item-product">
				<div class="item-product col-md-3 ">
						<div class="thumbnail">
							<a href="#"><img src="assets/img/fashion/6.png" alt=""></a>
							<button class="btn btn-primary  btn-quickview"> Quick View</button>
							<button class="btn btn-primary btn-addtocart">Add To Cart</button>
							<div class="caption">
								<h3 class="title-product text-center"><a href="#">Gack Pocket Square Gack Pocket Square</a></h3>
	                          	<div class="star text-center">
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star-half-empty"></span>
	                          	</div>
		                        <div class="price text-center">
		                            <div class="price-new">$50</div>
		                            <div class="price-old">$54</div>
		                        </div>
							</div>
						</div>
				</div>


				<div class="item-product col-md-3 ">
						<div class="thumbnail">
							<a href="#"><img src="assets/img/fashion/1.png" alt=""></a>
							<button class="btn btn-primary  btn-quickview"> Quick View</button>
							<button class="btn btn-primary btn-addtocart">Add To Cart</button>
							<div class="caption">
								<h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
	                          	<div class="star text-center">
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star-half-empty"></span>
	                          	</div>
		                        <div class="price text-center">
		                            <div class="price-new">$50</div>
		                            <div class="price-old">$54</div>
		                        </div>
							</div>
						</div>
				</div>
				<div class="item-product col-md-3 ">
						<div class="thumbnail">
							<a href="#"><img src="assets/img/fashion/5.png" alt=""></a>
							<button class="btn btn-primary  btn-quickview"> Quick View</button>
							<button class="btn btn-primary btn-addtocart">Add To Cart</button>
							<div class="caption">
								<h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
	                          	<div class="star text-center">
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star-half-empty"></span>
	                          	</div>
		                        <div class="price text-center">
		                            <div class="price-new">$50</div>
		                            <div class="price-old">$54</div>
		                        </div>
							</div>
						</div>
				</div>
				<div class="item-product col-md-3 ">
						<div class="thumbnail">
							<a href="#"><img src="assets/img/fashion/3.png" alt=""></a>
							<button class="btn btn-primary  btn-quickview"> Quick View</button>
							<button class="btn btn-primary btn-addtocart">Add To Cart</button>
							<div class="caption">
								<h3 class="title-product text-center"><a href="#">Gack Pocket Square Gack Pocket Square Gack Pocket Square</a></h3>
	                          	<div class="star text-center">
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star-half-empty"></span>
	                          	</div>
		                        <div class="price text-center">
		                            <div class="price-new">$50</div>
		                            <div class="price-old">$54</div>
		                        </div>
							</div>
						</div>
				</div>
				<div class="item-product col-md-3 ">
						<div class="thumbnail">
							<a href="#"><img src="assets/img/fashion/7.png" alt=""></a>
							<button class="btn btn-primary  btn-quickview"> Quick View</button>
							<button class="btn btn-primary btn-addtocart">Add To Cart</button>
							<div class="caption">
								<h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
	                          	<div class="star text-center">
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star-half-empty"></span>
	                          	</div>
		                        <div class="price text-center">
		                            <div class="price-new">$50</div>
		                            <div class="price-old">$54</div>
		                        </div>
							</div>
						</div>
				</div>
				<div class="item-product col-md-3 ">
						<div class="thumbnail">
							<a href="#"><img src="assets/img/fashion/1.png" alt=""></a>
							<button class="btn btn-primary  btn-quickview"> Quick View</button>
							<button class="btn btn-primary btn-addtocart">Add To Cart</button>
							<div class="caption">
								<h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
	                          	<div class="star text-center">
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star-half-empty"></span>
	                          	</div>
		                        <div class="price text-center">
		                            <div class="price-new">$50</div>
		                            <div class="price-old">$54</div>
		                        </div>
							</div>
						</div>
				</div>
				<div class="item-product col-md-3 ">
						<div class="thumbnail">
							<a href="#"><img src="assets/img/fashion/2.png" alt=""></a>
							<button class="btn btn-primary  btn-quickview"> Quick View</button>
							<button class="btn btn-primary btn-addtocart">Add To Cart</button>
							<div class="caption">
								<h3 class="title-product text-center"><a href="#">Gack Pocket Square  Pocket Square</a></h3>
	                          	<div class="star text-center">
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star-half-empty"></span>
	                          	</div>
		                        <div class="price text-center">
		                            <div class="price-new">$50</div>
		                            <div class="price-old">$54</div>
		                        </div>
							</div>
						</div>
				</div>
				<div class="item-product col-md-3 ">
						<div class="thumbnail">
							<a href="#"><img src="assets/img/fashion/4.png" alt=""></a>
							<button class="btn btn-primary  btn-quickview"> Quick View</button>
							<button class="btn btn-primary btn-addtocart">Add To Cart</button>
							<div class="caption">
								<h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
	                          	<div class="star text-center">
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star-half-empty"></span>
	                          	</div>
		                        <div class="price text-center">
		                            <div class="price-new">$50</div>
		                            <div class="price-old">$54</div>
		                        </div>
							</div>
						</div>
				</div>
				<div class="item-product col-md-3 ">
						<div class="thumbnail">
							<a href="#"><img src="assets/img/fashion/3.png" alt=""></a>
							<button class="btn btn-primary  btn-quickview"> Quick View</button>
							<button class="btn btn-primary btn-addtocart">Add To Cart</button>
							<div class="caption">
								<h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
	                          	<div class="star text-center">
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star-half-empty"></span>
	                          	</div>
		                        <div class="price text-center">
		                            <div class="price-new">$50</div>
		                            <div class="price-old">$54</div>
		                        </div>
							</div>
						</div>
				</div>
				<div class="item-product col-md-3 ">
						<div class="thumbnail">
							<a href="#"><img src="assets/img/fashion/7.png" alt=""></a>
							<button class="btn btn-primary  btn-quickview"> Quick View</button>
							<button class="btn btn-primary btn-addtocart">Add To Cart</button>
							<div class="caption">
								<h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
	                          	<div class="star text-center">
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star-half-empty"></span>
	                          	</div>
		                        <div class="price text-center">
		                            <div class="price-new">$50</div>
		                            <div class="price-old">$54</div>
		                        </div>
							</div>
						</div>
				</div>
				<div class="item-product col-md-3 ">
						<div class="thumbnail">
							<a href="#"><img src="assets/img/fashion/5.png" alt=""></a>
							<button class="btn btn-primary  btn-quickview"> Quick View</button>
							<button class="btn btn-primary btn-addtocart">Add To Cart</button>
							<div class="caption">
								<h3 class="title-product text-center"><a href="#">Gack Pocket Square Gack Pocket</a></h3>
	                          	<div class="star text-center">
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star-half-empty"></span>
	                          	</div>
		                        <div class="price text-center">
		                            <div class="price-new">$50</div>
		                            <div class="price-old">$54</div>
		                        </div>
							</div>
						</div>
				</div>
				<div class="item-product col-md-3 ">
						<div class="thumbnail">
							<a href="#"><img src="assets/img/fashion/1.png" alt=""></a>
							<button class="btn btn-primary  btn-quickview"> Quick View</button>
							<button class="btn btn-primary btn-addtocart">Add To Cart</button>
							<div class="caption">
								<h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
	                          	<div class="star text-center">
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star-half-empty"></span>
	                          	</div>
		                        <div class="price text-center">
		                            <div class="price-new">$50</div>
		                            <div class="price-old">$54</div>
		                        </div>
							</div>
						</div>
				</div>
				<div class="item-product col-md-3 ">
						<div class="thumbnail">
							<a href="#"><img src="assets/img/fashion/2.png" alt=""></a>
							<button class="btn btn-primary  btn-quickview"> Quick View</button>
							<button class="btn btn-primary btn-addtocart">Add To Cart</button>
							<div class="caption">
								<h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
	                          	<div class="star text-center">
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star-half-empty"></span>
	                          	</div>
		                        <div class="price text-center">
		                            <div class="price-new">$50</div>
		                            <div class="price-old">$54</div>
		                        </div>
							</div>
						</div>
				</div>
				<div class="item-product col-md-3 ">
						<div class="thumbnail">
							<a href="#"><img src="assets/img/fashion/3.png" alt=""></a>
							<button class="btn btn-primary  btn-quickview"> Quick View</button>
							<button class="btn btn-primary btn-addtocart">Add To Cart</button>
							<div class="caption">
								<h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
	                          	<div class="star text-center">
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star-half-empty"></span>
	                          	</div>
		                        <div class="price text-center">
		                            <div class="price-new">$50</div>
		                            <div class="price-old">$54</div>
		                        </div>
							</div>
						</div>
				</div>
				<div class="item-product col-md-3 ">
						<div class="thumbnail">
							<a href="#"><img src="assets/img/fashion/4.png" alt=""></a>
							<button class="btn btn-primary  btn-quickview"> Quick View</button>
							<button class="btn btn-primary btn-addtocart">Add To Cart</button>
							<div class="caption">
								<h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
	                          	<div class="star text-center">
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star-half-empty"></span>
	                          	</div>
		                        <div class="price text-center">
		                            <div class="price-new">$50</div>
		                            <div class="price-old">$54</div>
		                        </div>
							</div>
						</div>
				</div>

				<div class="item-product col-md-3 ">
						<div class="thumbnail">
							<a href="#"><img src="assets/img/fashion/5.png" alt=""></a>
							<button class="btn btn-primary  btn-quickview"> Quick View</button>
							<button class="btn btn-primary btn-addtocart">Add To Cart</button>
							<div class="caption">
								<h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
	                          	<div class="star text-center">
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star-half-empty"></span>
	                          	</div>
		                        <div class="price text-center">
		                            <div class="price-new">$50</div>
		                            <div class="price-old">$54</div>
		                        </div>
							</div>
						</div>
				</div>
				<div class="item-product col-md-3 ">
						<div class="thumbnail">
							<a href="#"><img src="assets/img/fashion/6.png" alt=""></a>
							<button class="btn btn-primary  btn-quickview"> Quick View</button>
							<button class="btn btn-primary btn-addtocart">Add To Cart</button>
							<div class="caption">
								<h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
	                          	<div class="star text-center">
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star-half-empty"></span>
	                          	</div>
		                        <div class="price text-center">
		                            <div class="price-new">$50</div>
		                            <div class="price-old">$54</div>
		                        </div>
							</div>
						</div>
				</div>

				<div class="item-product col-md-3 ">
						<div class="thumbnail">
							<a href="#"><img src="assets/img/fashion/7.png" alt=""></a>
							<button class="btn btn-primary  btn-quickview"> Quick View</button>
							<button class="btn btn-primary btn-addtocart">Add To Cart</button>
							<div class="caption">
								<h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
	                          	<div class="star text-center">
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star"></span>
	                            	<span class="fa fa-star-half-empty"></span>
	                          	</div>
		                        <div class="price text-center">
		                            <div class="price-new">$50</div>
		                            <div class="price-old">$54</div>
		                        </div>
							</div>
						</div>
				</div>
				
				
			</div>
		</div>
	</div>
</div>

