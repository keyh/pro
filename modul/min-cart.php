<div class="mini-cart">
			<div class="info-top">
				<p class="pull-left close-mini-cart"><i class="fa fa-times"></i></p>
				<p class="pull-right mini-shopping-cart"><i class=" fa fa-shopping-cart"></i><span>8</span></p>
			</div>
			<div class="body-sidebar">
				<ul class="list-unstyled">
					<li>
						<div class="media">
							<a class="pull-left" href="#">
								<img class="media-object" src="http://placehold.it/60x60" alt="Image">
							</a>
							<div class="media-body">
								<h5 class="media-heading title-product">Samsung Galaxy E5</h5>
								<span class="badge quality ">4</span>
								<span class="price ">$210</span>
								<span class="fa fa-times remove-product"></span>
							</div>
						</div>
					</li>
					<li>
						<div class="media">
							<a class="pull-left" href="#">
								<img class="media-object" src="http://placehold.it/60x60" alt="Image">
							</a>
							<div class="media-body">
								<h5 class="media-heading title-product">Samsung Galaxy E5</h5>
								<span class="badge quality ">4</span>
								<span class="price ">$210</span>
								<span class="fa fa-times remove-product"></span>
							</div>
						</div>
					</li>
					<li>
						<div class="media">
							<a class="pull-left" href="#">
								<img class="media-object" src="http://placehold.it/60x60" alt="Image">
							</a>
							<div class="media-body">
								<h5 class="media-heading title-product">Samsung Galaxy E5</h5>
								<span class="badge quality ">4</span>
								<span class="price ">$210</span>
								<span class="fa fa-times remove-product"></span>
							</div>
						</div>
					</li>
					<li>
						<div class="media">
							<a class="pull-left" href="#">
								<img class="media-object" src="http://placehold.it/60x60" alt="Image">
							</a>
							<div class="media-body">
								<h5 class="media-heading title-product">Samsung Galaxy E5</h5>
								<span class="badge quality ">4</span>
								<span class="price ">$210</span>
								<span class="fa fa-times remove-product"></span>
							</div>
						</div>
					</li>
					<li>
						<div class="media">
							<a class="pull-left" href="#">
								<img class="media-object" src="http://placehold.it/60x60" alt="Image">
							</a>
							<div class="media-body">
								<h5 class="media-heading title-product">Samsung Galaxy E5</h5>
								<span class="badge quality ">4</span>
								<span class="price ">$210</span>
								<span class="fa fa-times remove-product"></span>
							</div>
						</div>
					</li>
					<li>
						<div class="media">
							<a class="pull-left" href="#">
								<img class="media-object" src="http://placehold.it/60x60" alt="Image">
							</a>
							<div class="media-body">
								<h5 class="media-heading title-product">Samsung Galaxy E5</h5>
								<span class="badge quality ">4</span>
								<span class="price ">$210</span>
								<span class="fa fa-times remove-product"></span>
							</div>
						</div>
					</li>
				</ul>
			</div>
			<div class="info-bottom">
				<p class="total">Total <span>$778</span> </p>
				<a href="#">Kiểm tra</a>
			</div>
</div>