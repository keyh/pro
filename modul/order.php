<div class="container">
	<div class="order-product">
		<div role="tabpanel">
		    <!-- Nav tabs -->
		    <ul class="nav nav-tabs" role="tablist">
		        <li role="presentation" class="active">
		            <a href="#summary" aria-controls="home" role="tab" data-toggle="tab">Giỏ hàng</a>
		        </li>
		        <li role="presentation">
		            <a href="#sing-in" aria-controls="tab" role="tab" data-toggle="tab">Đăng nhập</a>
		        </li>
		        <li role="presentation">
		            <a href="#address" aria-controls="tab" role="tab" data-toggle="tab">Địa chỉ</a>
		        </li>
		        <li role="presentation">
		            <a href="#shipping" aria-controls="tab" role="tab" data-toggle="tab">Phí vận chuyển</a>
		        </li>
		        <li role="presentation">
		            <a href="#payment" aria-controls="tab" role="tab" data-toggle="tab">Thanh toán</a>
		        </li>
		    </ul>
		
		    <!-- Tab panes -->
		    <div class="tab-content">
		        <div role="tabpanel" class="tab-pane active" id="summary">
	        	    <table class="table view-cart">
				        <thead>
				            <tr>
				                <th></th>
				                <th></th>
				                <th>Tên sản phẩm</th>
				                <th>Giá sản phẩm</th>
				                <th>Số lượng</th>
				                <th>Tổng</th>
				            </tr>
				        </thead>
				        <tbody>
				            <tr>
				                <td><a href="#" class="remove-product"><i class="fa fa-times"></i></a></td>
				                <td><a href="#"><img src="assets/img/fashion/1.png" alt=""></a></td>
				                <td><a href="#" class="name-product">this is name fashion 1</a></td>
				                <td>$40</td>
				                <td><input type="number" class="quantity" value="2"> <a href="#" class="btn btn-primary"><i class="fa fa-refresh"></i></a> </td>
				                <td>$80</td>
				            </tr>
				             <tr>
				                <td><a href="#" class="remove-product"><i class="fa fa-times"></i></a></td>
				                <td><a href="#"><img src="assets/img/fashion/1.png" alt=""></a></td>
				                <td><a href="#" class="name-product">this is name fashion 1</a></td>
				                <td>$40</td>
				                <td><input type="number" class="quantity" value="2"> <a href="#" class="btn btn-primary"><i class="fa fa-refresh"></i></a></td>
				                <td>$80</td>
				            </tr>
				            <tr>
				                <td colspan="6">
				                    <div class="coupon pull-left">
				                        <input type="text" placeholder="Mã khuyến mãi">
				                        <div class="btn btn-default btn-coupon">Apply</div>
				                    </div>

				                </td>
				                
				            </tr>
				        </tbody>
				    </table>
				    <div class="row">
				        <div class="col-md-6 col-md-offset-6">
				        <h3>Tổng:</h3>
				            <table class="table">
				                <tbody>
				                    <tr>
				                        <th>Tổng phụ</th>
				                        <td>$80</td>
				                    </tr>
				                    <tr>
				                        <th>Phí vận chuyển</th>
				                        <td>Miễn phí vận chuyển</td>
				                    </tr>
				                    <tr>
				                        <th>Tổng</th>
				                        <th>$80</th>
				                    </tr>
				                </tbody>
				            </table>
				            <div class="check-out pull-right">
				                <a href="#" class="btn btn-primary">Thanh toán <i class="fa fa-arrow-circle-o-right"></i></a>
				            </div>
				        </div>
				    </div>
		        </div>
		        <div role="tabpanel" class="tab-pane" id="sing-in">
		        	<div class="row">
		        		<div class="col-md-6 register">
		        			<h4>Bạn là khách hàng mới? </h4>
		        			<p>Đăng ký với chúng tôi để thanh toán nhanh hơn, để theo dõi tình trạng đặt hàng của bạn và nhiều hơn nữa. Bạn cũng có thể thanh toán mà không cần đăng ký tài khoản.</p>
		        			<button class="btn btn-primary">Đăng ký tài khoản mới</button>
		        			<button class="btn btn-primary">Đặt hàng không cần tài khoản</button>
		        		</div>
		        		<div class="col-md-6 login">
		        			<h4>Bạn đã có tài khoản trên hệ thống</h4>
		        			<p>Để tiếp tục, vui lòng nhập địa chỉ email và mật khẩu mà bạn sử dụng cho tài khoản của bạn.</p>
		        			<form action="" method="POST" role="form">
		        				<div class="form-group">
		        					<label for="">Email</label>
		        					<input type="text" class="form-control" id="" placeholder="Email ">
		        				</div>
		        				<div class="form-group">
		        					<label for="">Mật khẩu</label>
		        					<input type="password" class="form-control" id="" placeholder="Mật khẩu">
		        				</div>
		        				<button type="submit" class="btn btn-primary">Submit</button>
		        			</form>
		        		</div>
		        	</div>
		        </div>
		        <div role="tabpanel" class="tab-pane" id="address">
		        	<form action="" method="POST" role="form">
		        	
		        		<div class="form-group">
		        			<label for="">Email</label>
		        			<input type="email" class="form-control" id="" placeholder="Input field">
		        		</div>
		        		<div class="form-group">
		        			<label for="">Họ và tên</label>
		        			<input type="email" class="form-control" id="" placeholder="Input field">
		        		</div>
		        		<div class="form-group">
		        			<label for="">Điện thoại</label>
		        			<input type="email" class="form-control" id="" placeholder="Input field">
		        		</div>
		        		<div class="form-group">
		        			<label for="">Địa chỉ</label>
		        			<input type="email" class="form-control" id="" placeholder="Input field">
		        		</div>
		        		<div class="form-group">
		        			<label for="">Tỉnh/ Thành phố</label>
		        			<input type="email" class="form-control" id="" placeholder="Input field">
		        		</div>
		        		
		        		<button type="submit" class="btn btn-primary">Submit</button>
		        	</form>
		        </div>
		        <div role="tabpanel" class="tab-pane" id="shipping">
		        	<h4>Hình thức vận chuyển đơn hàng</h4>
		        	<p><input type="radio" checked="checked"> Miễn phí vận chuyển và lắp đặt trong nội thành Hà Nội với đơn hàng trên 500.000 VND (Trong ngày) </p>
		        </div>
		        <div role="tabpanel" class="tab-pane" id="payment">
		        	
		        </div>
		    </div>
		</div>
	</div>
</div>