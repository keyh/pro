<div class="container category-c3">	
		<div class="category-c3-right">
			<div class=" row">
				<div class="col-md-12 sort-product">
					<p class="pull-left"> Hiển thị 1-9 của 300 sản phẩm</p>
					<form action="" class="pull-right">
						<div class="form-group">
							<select class="form-control" id="select">
				                <option>Sắp xếp theo tên</option>
				                <option>Sắp xếp theo giá</option>
				                <option>Sắp xếp theo rating</option>
				                <option>Sắp xếp theo phổ biến</option>
				                <option>Sắp xếp theo mới nhất</option>
				            </select>
			        	</div>
			        </form>
		    	</div>
			</div>
			<div id="masonry-cat3" class="row list-item-product">
				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 item-product col-masonry-cat3">
					<div class="thumbnail">
						<a href="#"><img src="assets/img/fashion/1.png" alt=""></a>
						<div class="list-button">
                             <a href="#"> <i class="fa fa-line-chart"> </i></a>
                             <a data-toggle="modal" href='#modal-quickview'> <i class="fa fa-search"> </i></a>
                             <a data-toggle="modal" href='#modal-addtocart'> <i class="fa fa-shopping-cart"> </i></a>
                             <a href="#"> <i class="fa fa-heart"> </i></a>
                             <a href="#"> <i class="fa fa-exchange"> </i></a>
                        </div>
						<div class="caption">
							<h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
                          	<div class="star text-center">
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star-half-empty"></span>
                          	</div>
	                        <div class="price text-center">
	                            <div class="price-new">$50</div>
	                            <div class="price-old">$54</div>
	                        </div>
						</div>
					</div>
				</div>
				
				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 item-product col-masonry-cat3">
					<div class="thumbnail">
						<a href="#"><img src="assets/img/fashion/1.png" alt=""></a>
						<div class="list-button">
                             <a href="#"> <i class="fa fa-line-chart"> </i></a>
                                <a data-toggle="modal" href='#modal-quickview'> <i class="fa fa-search"> </i></a>
                             <a data-toggle="modal" href='#modal-addtocart'> <i class="fa fa-shopping-cart"> </i></a>
                             <a href="#"> <i class="fa fa-heart"> </i></a>
                             <a href="#"> <i class="fa fa-exchange"> </i></a>
                        </div>
						<div class="caption">
							<h3 class="title-product text-center"><a href="#">Gack Pocket Square Gack Pocket Square Gack Pocket Squareb Gack Pocket Square Pocket Square Gack Pocket Squareb Gack Pocket Square</a></h3>
                          	<div class="star text-center">
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star-half-empty"></span>
                          	</div>
	                        <div class="price text-center">
	                            <div class="price-new">$50</div>
	                            <div class="price-old">$54</div>
	                        </div>
						</div>
					</div>
				</div>

				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 item-product col-masonry-cat3">
					<div class="thumbnail">
						<a href="#"><img src="assets/img/fashion/1.png" alt=""></a>
						<div class="list-button">
                             <a href="#"> <i class="fa fa-line-chart"> </i></a>
                                <a data-toggle="modal" href='#modal-quickview'> <i class="fa fa-search"> </i></a>
                             <a data-toggle="modal" href='#modal-addtocart'> <i class="fa fa-shopping-cart"> </i></a>
                             <a href="#"> <i class="fa fa-heart"> </i></a>
                             <a href="#"> <i class="fa fa-exchange"> </i></a>
                        </div>
						<div class="caption">
							<h3 class="title-product text-center"><a href="#">Gack Pocket Square Pocket Square</a></h3>
                          	<div class="star text-center">
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star-half-empty"></span>
                          	</div>
	                        <div class="price text-center">
	                            <div class="price-new">$50</div>
	                            <div class="price-old">$54</div>
	                        </div>
						</div>
					</div>
				</div>

				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 item-product col-masonry-cat3">
					<div class="thumbnail">
						<a href="#"><img src="assets/img/fashion/1.png" alt=""></a>
						<div class="list-button">
                             <a href="#"> <i class="fa fa-line-chart"> </i></a>
                                <a data-toggle="modal" href='#modal-quickview'> <i class="fa fa-search"> </i></a>
                             <a data-toggle="modal" href='#modal-addtocart'> <i class="fa fa-shopping-cart"> </i></a>
                             <a href="#"> <i class="fa fa-heart"> </i></a>
                             <a href="#"> <i class="fa fa-exchange"> </i></a>
                        </div>
						<div class="caption">
							<h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
                          	<div class="star text-center">
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star-half-empty"></span>
                          	</div>
	                        <div class="price text-center">
	                            <div class="price-new">$50</div>
	                            <div class="price-old">$54</div>
	                        </div>
						</div>
					</div>
				</div>

				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 item-product col-masonry-cat3">
					<div class="thumbnail">
						<a href="#"><img src="assets/img/fashion/1.png" alt=""></a>
						<div class="list-button">
                             <a href="#"> <i class="fa fa-line-chart"> </i></a>
                                <a data-toggle="modal" href='#modal-quickview'> <i class="fa fa-search"> </i></a>
                             <a data-toggle="modal" href='#modal-addtocart'> <i class="fa fa-shopping-cart"> </i></a>
                             <a href="#"> <i class="fa fa-heart"> </i></a>
                             <a href="#"> <i class="fa fa-exchange"> </i></a>
                        </div>
						<div class="caption">
							<h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
                          	<div class="star text-center">
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star-half-empty"></span>
                          	</div>
	                        <div class="price text-center">
	                            <div class="price-new">$50</div>
	                            <div class="price-old">$54</div>
	                        </div>
						</div>
					</div>
				</div>

				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 item-product col-masonry-cat3">
					<div class="thumbnail">
						<a href="#"><img src="assets/img/fashion/1.png" alt=""></a>
						<div class="list-button">
                             <a href="#"> <i class="fa fa-line-chart"> </i></a>
                                <a data-toggle="modal" href='#modal-quickview'> <i class="fa fa-search"> </i></a>
                             <a data-toggle="modal" href='#modal-addtocart'> <i class="fa fa-shopping-cart"> </i></a>
                             <a href="#"> <i class="fa fa-heart"> </i></a>
                             <a href="#"> <i class="fa fa-exchange"> </i></a>
                        </div>
						<div class="caption">
							<h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
                          	<div class="star text-center">
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star-half-empty"></span>
                          	</div>
	                        <div class="price text-center">
	                            <div class="price-new">$50</div>
	                            <div class="price-old">$54</div>
	                        </div>
						</div>
					</div>
				</div>

				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 item-product col-masonry-cat3">
					<div class="thumbnail">
						<a href="#"><img src="assets/img/fashion/1.png" alt=""></a>
						<div class="list-button">
                             <a href="#"> <i class="fa fa-line-chart"> </i></a>
                                <a data-toggle="modal" href='#modal-quickview'> <i class="fa fa-search"> </i></a>
                             <a data-toggle="modal" href='#modal-addtocart'> <i class="fa fa-shopping-cart"> </i></a>
                             <a href="#"> <i class="fa fa-heart"> </i></a>
                             <a href="#"> <i class="fa fa-exchange"> </i></a>
                        </div>
						<div class="caption">
							<h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
                          	<div class="star text-center">
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star-half-empty"></span>
                          	</div>
	                        <div class="price text-center">
	                            <div class="price-new">$50</div>
	                            <div class="price-old">$54</div>
	                        </div>
						</div>
					</div>
				</div>

				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 item-product col-masonry-cat3">
					<div class="thumbnail">
						<a href="#"><img src="assets/img/fashion/1.png" alt=""></a>
						<div class="list-button">
                             <a href="#"> <i class="fa fa-line-chart"> </i></a>
                                <a data-toggle="modal" href='#modal-quickview'> <i class="fa fa-search"> </i></a>
                             <a data-toggle="modal" href='#modal-addtocart'> <i class="fa fa-shopping-cart"> </i></a>
                             <a href="#"> <i class="fa fa-heart"> </i></a>
                             <a href="#"> <i class="fa fa-exchange"> </i></a>
                        </div>
						<div class="caption">
							<h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
                          	<div class="star text-center">
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star-half-empty"></span>
                          	</div>
	                        <div class="price text-center">
	                            <div class="price-new">$50</div>
	                            <div class="price-old">$54</div>
	                        </div>
						</div>
					</div>
				</div>

				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 item-product col-masonry-cat3">
					<div class="thumbnail">
						<a href="#"><img src="assets/img/fashion/1.png" alt=""></a>
						<div class="list-button">
                             <a href="#"> <i class="fa fa-line-chart"> </i></a>
                                <a data-toggle="modal" href='#modal-quickview'> <i class="fa fa-search"> </i></a>
                             <a data-toggle="modal" href='#modal-addtocart'> <i class="fa fa-shopping-cart"> </i></a>
                             <a href="#"> <i class="fa fa-heart"> </i></a>
                             <a href="#"> <i class="fa fa-exchange"> </i></a>
                        </div>
						<div class="caption">
							<h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
                          	<div class="star text-center">
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star-half-empty"></span>
                          	</div>
	                        <div class="price text-center">
	                            <div class="price-new">$50</div>
	                            <div class="price-old">$54</div>
	                        </div>
						</div>
					</div>
				</div>

				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 item-product col-masonry-cat3">
					<div class="thumbnail">
						<a href="#"><img src="assets/img/fashion/1.png" alt=""></a>
						<div class="list-button">
                             <a href="#"> <i class="fa fa-line-chart"> </i></a>
                                <a data-toggle="modal" href='#modal-quickview'> <i class="fa fa-search"> </i></a>
                             <a data-toggle="modal" href='#modal-addtocart'> <i class="fa fa-shopping-cart"> </i></a>
                             <a href="#"> <i class="fa fa-heart"> </i></a>
                             <a href="#"> <i class="fa fa-exchange"> </i></a>
                        </div>
						<div class="caption">
							<h3 class="title-product text-center"><a href="#">Gack Pocket Square Pocket Square</a></h3>
                          	<div class="star text-center">
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star-half-empty"></span>
                          	</div>
	                        <div class="price text-center">
	                            <div class="price-new">$50</div>
	                            <div class="price-old">$54</div>
	                        </div>
						</div>
					</div>
				</div>

				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 item-product col-masonry-cat3">
					<div class="thumbnail">
						<a href="#"><img src="assets/img/fashion/1.png" alt=""></a>
						<div class="list-button">
                             <a href="#"> <i class="fa fa-line-chart"> </i></a>
                                <a data-toggle="modal" href='#modal-quickview'> <i class="fa fa-search"> </i></a>
                             <a data-toggle="modal" href='#modal-addtocart'> <i class="fa fa-shopping-cart"> </i></a>
                             <a href="#"> <i class="fa fa-heart"> </i></a>
                             <a href="#"> <i class="fa fa-exchange"> </i></a>
                        </div>
						<div class="caption">
							<h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
                          	<div class="star text-center">
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star-half-empty"></span>
                          	</div>
	                        <div class="price text-center">
	                            <div class="price-new">$50</div>
	                            <div class="price-old">$54</div>
	                        </div>
						</div>
					</div>
				</div>

				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 item-product col-masonry-cat3">
					<div class="thumbnail">
						<a href="#"><img src="assets/img/fashion/1.png" alt=""></a>
						<div class="list-button">
                             <a href="#"> <i class="fa fa-line-chart"> </i></a>
                                <a data-toggle="modal" href='#modal-quickview'> <i class="fa fa-search"> </i></a>
                             <a data-toggle="modal" href='#modal-addtocart'> <i class="fa fa-shopping-cart"> </i></a>
                             <a href="#"> <i class="fa fa-heart"> </i></a>
                             <a href="#"> <i class="fa fa-exchange"> </i></a>
                        </div>
						<div class="caption">
							<h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
                          	<div class="star text-center">
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star-half-empty"></span>
                          	</div>
	                        <div class="price text-center">
	                            <div class="price-new">$50</div>
	                            <div class="price-old">$54</div>
	                        </div>
						</div>
					</div>
				</div>

				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 item-product col-masonry-cat3">
					<div class="thumbnail">
						<a href="#"><img src="assets/img/fashion/1.png" alt=""></a>
						<div class="list-button">
                             <a href="#"> <i class="fa fa-line-chart"> </i></a>
                                <a data-toggle="modal" href='#modal-quickview'> <i class="fa fa-search"> </i></a>
                             <a data-toggle="modal" href='#modal-addtocart'> <i class="fa fa-shopping-cart"> </i></a>
                             <a href="#"> <i class="fa fa-heart"> </i></a>
                             <a href="#"> <i class="fa fa-exchange"> </i></a>
                        </div>
						<div class="caption">
							<h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
                          	<div class="star text-center">
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star-half-empty"></span>
                          	</div>
	                        <div class="price text-center">
	                            <div class="price-new">$50</div>
	                            <div class="price-old">$54</div>
	                        </div>
						</div>
					</div>
				</div>

				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 item-product col-masonry-cat3">
					<div class="thumbnail">
						<a href="#"><img src="assets/img/fashion/1.png" alt=""></a>
						<div class="list-button">
                             <a href="#"> <i class="fa fa-line-chart"> </i></a>
                                <a data-toggle="modal" href='#modal-quickview'> <i class="fa fa-search"> </i></a>
                             <a data-toggle="modal" href='#modal-addtocart'> <i class="fa fa-shopping-cart"> </i></a>
                             <a href="#"> <i class="fa fa-heart"> </i></a>
                             <a href="#"> <i class="fa fa-exchange"> </i></a>
                        </div>
						<div class="caption">
							<h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
                          	<div class="star text-center">
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star-half-empty"></span>
                          	</div>
	                        <div class="price text-center">
	                            <div class="price-new">$50</div>
	                            <div class="price-old">$54</div>
	                        </div>
						</div>
					</div>
				</div>

				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 item-product col-masonry-cat3">
					<div class="thumbnail">
						<a href="#"><img src="assets/img/fashion/1.png" alt=""></a>
						<div class="list-button">
                             <a href="#"> <i class="fa fa-line-chart"> </i></a>
                                <a data-toggle="modal" href='#modal-quickview'> <i class="fa fa-search"> </i></a>
                             <a data-toggle="modal" href='#modal-addtocart'> <i class="fa fa-shopping-cart"> </i></a>
                             <a href="#"> <i class="fa fa-heart"> </i></a>
                             <a href="#"> <i class="fa fa-exchange"> </i></a>
                        </div>
						<div class="caption">
							<h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
                          	<div class="star text-center">
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star-half-empty"></span>
                          	</div>
	                        <div class="price text-center">
	                            <div class="price-new">$50</div>
	                            <div class="price-old">$54</div>
	                        </div>
						</div>
					</div>
				</div>

				<div class="col-xs-3 col-sm-3 col-md-3 col-lg-3 item-product col-masonry-cat3">
					<div class="thumbnail">
						<a href="#"><img src="assets/img/fashion/1.png" alt=""></a>
						<div class="list-button">
                             <a href="#"> <i class="fa fa-line-chart"> </i></a>
                                <a data-toggle="modal" href='#modal-quickview'> <i class="fa fa-search"> </i></a>
                             <a data-toggle="modal" href='#modal-addtocart'> <i class="fa fa-shopping-cart"> </i></a>
                             <a href="#"> <i class="fa fa-heart"> </i></a>
                             <a href="#"> <i class="fa fa-exchange"> </i></a>
                        </div>
						<div class="caption">
							<h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
                          	<div class="star text-center">
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star"></span>
                            	<span class="fa fa-star-half-empty"></span>
                          	</div>
	                        <div class="price text-center">
	                            <div class="price-new">$50</div>
	                            <div class="price-old">$54</div>
	                        </div>
						</div>
					</div>
				</div>
				
			</div>
		</div>
</div>