  <div class="container">
    <div class="row banner">
      <div class="col-md-3 sidebar-left po-relative">
        <h2><i class=" fa fa-bars"> Chuyên Mục</i></h2>
        <ul class="list-unstyled main-category ">
            <li>
                <a href="#"><i class="fa fa-desktop"></i>Gia dụng</a>
                <span class="fa fa-angle-right pull-right"></span>
                <div class="child-category">
                    <div class="col-sm-12">
                      <div class="mega-group col-sm-4">
                          <h4 class="mega-group-header"><span>Tennis</span></h4>
                          <ul class="list-unstyled">
                              <li><a href="#">Tennis</a></li>
                              <li><a href="#">Coats &amp; Jackets</a></li>
                              <li><a href="#">Blouses &amp; Shirts</a></li>
                              <li><a href="#">Tops &amp; Tees</a></li>
                              <li><a href="#">Hoodies &amp; Sweatshirts</a></li>
                              <li><a href="#">Intimates</a></li>
                          </ul>
                      </div>
                      <div class="mega-group col-sm-4">
                          <h4 class="mega-group-header"><span>Swimming</span></h4>
                          <ul class="list-unstyled">
                              <li><a href="#">Dresses</a></li>
                              <li><a href="#">Coats &amp; Jackets</a></li>
                              <li><a href="#">Blouses &amp; Shirts</a></li>
                              <li><a href="#">Tops &amp; Tees</a></li>
                              <li><a href="#">Hoodies &amp; Sweatshirts</a></li>
                              <li><a href="#">Intimates</a></li>
                          </ul>
                      </div>
                      <div class="mega-group col-sm-4">
                          <h4 class="mega-group-header"><span>Shoes</span></h4>
                          <ul class="list-unstyled">
                              <li><a href="#">Dresses</a></li>
                              <li><a href="#">Coats &amp; Jackets</a></li>
                              <li><a href="#">Blouses &amp; Shirts</a></li>
                              <li><a href="#">Tops &amp; Tees</a></li>
                              <li><a href="#">Hoodies &amp; Sweatshirts</a></li>
                              <li><a href="#">Intimates</a></li>
                          </ul>
                      </div>
                      
                    </div>
                </div>
            </li>
            <li><a href="#"><i class="fa fa-desktop"></i>Điện tử</a></li>
            <li><a href="#"><i class="fa fa-desktop"></i>Thời trang</a></li>
            <li><a href="#"><i class="fa fa-desktop"></i>Nhà cửa - Trang trí</a></li>
            <li><a href="#"><i class="fa fa-desktop"></i>Sách</a></li>
            <li><a href="#"><i class="fa fa-desktop"></i>Thể thao</a></li>
            <li><a href="#"><i class="fa fa-desktop"></i>Điện gia dụng</a></li>
            <li><a href="#"><i class="fa fa-desktop"></i>Văn phòng</a></li>
            <li><a href="#"><i class="fa fa-desktop"></i>Điện thoại</a></li>  
            <li><a href="#"><i class="fa fa-desktop"></i>Máy tính</a></li>  
        </ul>
      </div>
      <div class="col-md-9 hide-sm">
        <div id="carousel-banner" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#carousel-banner" data-slide-to="0" class=""></li>
                <li data-target="#carousel-banner" data-slide-to="1" class="active"></li>
               
            </ol>
            <div class="carousel-inner">
                <div class="item">
                  <img src="assets/img/banner/1.jpg" alt="">
                    <div class="container">
                        <div class="carousel-caption">
                            <h1>Example headline.</h1>
                            <p>Note: If you're viewing this page via a <code>file://</code> URL, the "next" and "previous" Glyphicon buttons on the left and right might not load/display properly due to web browser security rules.</p>
                        </div>
                    </div>
                </div>
                <div class="item active">
                  <img src="assets/img/banner/2.jpg" alt="">
                    <div class="container">
                        <div class="carousel-caption">
                            <h1>Another example headline.</h1>
                            <p>Cras justo odio, dapibus ac facilisis in, egestas eget quam. Donec id elit non mi porta gravida at eget metus. Nullam id dolor id nibh ultricies vehicula ut id elit.</p>
                        </div>
                    </div>
                </div>
            </div>
            <a class="left carousel-control" href="#carousel-banner" data-slide="prev"> <i class="fa fa-long-arrow-left"></i></a>
            <a class="right carousel-control" href="#carousel-banner" data-slide="next"> <i class="fa fa-long-arrow-right"></i> </a>
        </div>
      </div>
    </div>
      <div class="category-product">
          <div class="title-category">
              <a href="#"> <span>Thời trang</span> </a>
          </div>
          <div class="jcarousel-wrapper">
              <div class="jcarousel">
                  <ul class="list-item-product">
                      <li class="item-product">
                          <a href="#"><img src="assets/img/fashion/1.png" alt="Image 1"></a>
                          <div class="list-button">
                             <a href="#"> <i class="fa fa-line-chart"> </i></a>
                                <a data-toggle="modal" href='#modal-quickview'> <i class="fa fa-search"> </i></a>
                             <a data-toggle="modal" href='#modal-addtocart'> <i class="fa fa-shopping-cart"> </i></a>
                             <a href="#"> <i class="fa fa-heart"> </i></a>
                             <a href="#"> <i class="fa fa-exchange"> </i></a>
                          </div>
                          <h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
                          <div class="star text-center">
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star-half-empty"></span>
                          </div>
                          <div class="price text-center">
                            <div class="price-new">$50</div>
                            <div class="price-old">$54</div>
                          </div>
                          
                      </li>
                      <li class="item-product">
                          <a href="#"><img src="assets/img/fashion/2.png" alt="Image 1"></a>
                          <div class="list-button">
                             <a href="#"> <i class="fa fa-line-chart"> </i></a>
                             <a href="#"> <i class="fa fa-search"> </i></a>
                             <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
                             <a href="#"> <i class="fa fa-heart"> </i></a>
                             <a href="#"> <i class="fa fa-exchange"> </i></a>
                          </div>
                          <h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
                          <div class="star text-center">
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star-half-empty"></span>
                          </div>
                          <div class="price text-center">
                            <div class="price-new">$50</div>
                            <div class="price-old">$54</div>
                          </div>
                      </li>
                      <li class="item-product">
                          <a href="#"><img src="assets/img/fashion/3.png" alt="Image 1"></a>
                          <div class="list-button">
                             <a href="#"> <i class="fa fa-line-chart"> </i></a>
                             <a href="#"> <i class="fa fa-search"> </i></a>
                             <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
                             <a href="#"> <i class="fa fa-heart"> </i></a>
                             <a href="#"> <i class="fa fa-exchange"> </i></a>
                          </div>
                          <h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
                          <div class="star text-center">
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star-half-empty"></span>
                          </div>
                          <div class="price text-center">
                            <div class="price-new">$50</div>
                            <div class="price-old">$54</div>
                          </div>
                      </li>
                      <li class="item-product">
                          <a href="#"><img src="assets/img/fashion/4.png" alt="Image 1"></a>
                          <div class="list-button">
                             <a href="#"> <i class="fa fa-line-chart"> </i></a>
                             <a href="#"> <i class="fa fa-search"> </i></a>
                             <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
                             <a href="#"> <i class="fa fa-heart"> </i></a>
                             <a href="#"> <i class="fa fa-exchange"> </i></a>
                          </div>
                          <h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
                          <div class="star text-center">
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star-half-empty"></span>
                          </div>
                          <div class="price text-center">
                            <div class="price-new">$50</div>
                            <div class="price-old">$54</div>
                          </div>
                      </li>
                      <li class="item-product">
                          <a href="#"><img src="assets/img/fashion/5.png" alt="Image 1"></a>
                          <div class="list-button">
                             <a href="#"> <i class="fa fa-line-chart"> </i></a>
                             <a href="#"> <i class="fa fa-search"> </i></a>
                             <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
                             <a href="#"> <i class="fa fa-heart"> </i></a>
                             <a href="#"> <i class="fa fa-exchange"> </i></a>
                          </div>
                          <h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
                          <div class="star text-center">
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star-half-empty"></span>
                          </div>
                          <div class="price text-center">
                            <div class="price-new">$50</div>
                            <div class="price-old">$54</div>
                          </div>
                      </li>
                      <li class="item-product">
                          <a href="#"><img src="assets/img/fashion/6.png" alt="Image 1"></a>
                          <div class="list-button">
                             <a href="#"> <i class="fa fa-line-chart"> </i></a>
                             <a href="#"> <i class="fa fa-search"> </i></a>
                             <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
                             <a href="#"> <i class="fa fa-heart"> </i></a>
                             <a href="#"> <i class="fa fa-exchange"> </i></a>
                          </div>
                          <h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
                          <div class="star text-center">
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star-half-empty"></span>
                          </div>
                          <div class="price text-center">
                            <div class="price-new">$50</div>
                            <div class="price-old">$54</div>
                          </div>
                      </li>
                  </ul>
              </div>

              <a href="javascript:void(0)" class="jcarousel-control-prev">&lsaquo;</a>
              <a href="javascript:void(0)" class="jcarousel-control-next">&rsaquo;</a>
          </div>
      </div>

      <div class="category-product">
          <div class="title-category">
              <a href="#"> <span>Đồng hồ nam</span> </a>
          </div>
          <div class="jcarousel-wrapper">
              <div class="jcarousel">
                  <ul class="list-item-product">
                      <li class="item-product">
                          <a href="#"><img src="assets/img/fashion/c1.jpg" alt=""></a>
                          <div class="list-button">
                             <a href="#"> <i class="fa fa-line-chart"> </i></a>
                             <a href="#"> <i class="fa fa-search"> </i></a>
                             <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
                             <a href="#"> <i class="fa fa-heart"> </i></a>
                             <a href="#"> <i class="fa fa-exchange"> </i></a>
                          </div>
                          <h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
                          <div class="star text-center">
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star-half-empty"></span>
                          </div>
                          <div class="price text-center">
                            <div class="price-new">$50</div>
                            <div class="price-old">$54</div>
                          </div>
                      </li>
                      <li class="item-product">
                          <a href="#"><img src="assets/img/fashion/c2.jpg" alt=""></a>
                          <div class="list-button">
                             <a href="#"> <i class="fa fa-line-chart"> </i></a>
                             <a href="#"> <i class="fa fa-search"> </i></a>
                             <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
                             <a href="#"> <i class="fa fa-heart"> </i></a>
                             <a href="#"> <i class="fa fa-exchange"> </i></a>
                          </div>
                          <h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
                          <div class="star text-center">
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star-half-empty"></span>
                          </div>
                          <div class="price text-center">
                            <div class="price-new">$50</div>
                            <div class="price-old">$54</div>
                          </div>
                      </li>
                      <li class="item-product">
                          <a href="#"><img src="assets/img/fashion/c3.jpg" alt=""></a>
                          <div class="list-button">
                             <a href="#"> <i class="fa fa-line-chart"> </i></a>
                             <a href="#"> <i class="fa fa-search"> </i></a>
                             <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
                             <a href="#"> <i class="fa fa-heart"> </i></a>
                             <a href="#"> <i class="fa fa-exchange"> </i></a>
                          </div>
                          <h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
                          <div class="star text-center">
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star-half-empty"></span>
                          </div>
                          <div class="price text-center">
                            <div class="price-new">$50</div>
                            <div class="price-old">$54</div>
                          </div>
                      </li>
                      <li class="item-product">
                          <a href="#"><img src="assets/img/fashion/c4.jpg" alt=""></a>
                          <div class="list-button">
                             <a href="#"> <i class="fa fa-line-chart"> </i></a>
                             <a href="#"> <i class="fa fa-search"> </i></a>
                             <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
                             <a href="#"> <i class="fa fa-heart"> </i></a>
                             <a href="#"> <i class="fa fa-exchange"> </i></a>
                          </div>
                          <h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
                          <div class="star text-center">
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star-half-empty"></span>
                          </div>
                          <div class="price text-center">
                            <div class="price-new">$50</div>
                            <div class="price-old">$54</div>
                          </div>
                      </li>
                      <li class="item-product">
                          <a href="#"><img src="assets/img/fashion/c5.jpg" alt=""></a>
                          <div class="list-button">
                             <a href="#"> <i class="fa fa-line-chart"> </i></a>
                             <a href="#"> <i class="fa fa-search"> </i></a>
                             <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
                             <a href="#"> <i class="fa fa-heart"> </i></a>
                             <a href="#"> <i class="fa fa-exchange"> </i></a>
                          </div>
                          <h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
                          <div class="star text-center">
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star-half-empty"></span>
                          </div>
                          <div class="price text-center">
                            <div class="price-new">$50</div>
                            <div class="price-old">$54</div>
                          </div>
                      </li>
                      <li class="item-product">
                          <a href="#"><img src="assets/img/fashion/c6.jpg" alt=""></a>
                          <div class="list-button">
                             <a href="#"> <i class="fa fa-line-chart"> </i></a>
                             <a href="#"> <i class="fa fa-search"> </i></a>
                             <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
                             <a href="#"> <i class="fa fa-heart"> </i></a>
                             <a href="#"> <i class="fa fa-exchange"> </i></a>
                          </div>
                          <h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
                          <div class="star text-center">
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star-half-empty"></span>
                          </div>
                          <div class="price text-center">
                            <div class="price-new">$50</div>
                            <div class="price-old">$54</div>
                          </div>
                      </li>
                  </ul>
              </div>

              <a href="javascript:void(0)" class="jcarousel-control-prev">&lsaquo;</a>
              <a href="javascript:void(0)" class="jcarousel-control-next">&rsaquo;</a>
          </div>
      </div>
      
      <section class="section section-advertising clearfix">
          <div class="free-shipping pull-left text-center">
              <img src="assets/img/car.png" alt="">
              <p>Miễn phí vận chuyển</p>
          </div>
          <div class="custom-care pull-left text-center">
              <img src="assets/img/help.png" alt="">
              <p>Chăm sóc khách hàng</p>
          </div>
          <div class="party pull-left text-center">
              <img src="assets/img/party.png" alt="">
              <p>Quà tặng hấp dẫn</p>
          </div>
      </section>

      <div class="category-product">
          <div class="title-category">
              <a href="#"> <span>Giầy thời trang</span> </a>
          </div>
          <div class="jcarousel-wrapper">
              <div class="jcarousel">
                  <ul class="list-item-product">
                      <li class="item-product">
                          <a href="#"><img src="assets/img/fashion/s1.jpg" alt=""></a>
                          <div class="list-button">
                             <a href="#"> <i class="fa fa-line-chart"> </i></a>
                             <a href="#"> <i class="fa fa-search"> </i></a>
                             <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
                             <a href="#"> <i class="fa fa-heart"> </i></a>
                             <a href="#"> <i class="fa fa-exchange"> </i></a>
                          </div>
                          <h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
                          <div class="star text-center">
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star-half-empty"></span>
                          </div>
                          <div class="price text-center">
                            <div class="price-new">$50</div>
                            <div class="price-old">$54</div>
                          </div>
                      </li>
                      <li class="item-product">
                          <a href="#"><img src="assets/img/fashion/s2.jpg" alt=""></a>
                          <div class="list-button">
                             <a href="#"> <i class="fa fa-line-chart"> </i></a>
                             <a href="#"> <i class="fa fa-search"> </i></a>
                             <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
                             <a href="#"> <i class="fa fa-heart"> </i></a>
                             <a href="#"> <i class="fa fa-exchange"> </i></a>
                          </div>
                          <h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
                          <div class="star text-center">
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star-half-empty"></span>
                          </div>
                          <div class="price text-center">
                            <div class="price-new">$50</div>
                            <div class="price-old">$54</div>
                          </div>
                      </li>
                      <li class="item-product">
                          <a href="#"><img src="assets/img/fashion/s3.jpg" alt=""></a>
                          <div class="list-button">
                             <a href="#"> <i class="fa fa-line-chart"> </i></a>
                             <a href="#"> <i class="fa fa-search"> </i></a>
                             <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
                             <a href="#"> <i class="fa fa-heart"> </i></a>
                             <a href="#"> <i class="fa fa-exchange"> </i></a>
                          </div>
                          <h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
                          <div class="star text-center">
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star-half-empty"></span>
                          </div>
                          <div class="price text-center">
                            <div class="price-new">$50</div>
                            <div class="price-old">$54</div>
                          </div>
                      </li>
                      <li class="item-product">
                          <a href="#"><img src="assets/img/fashion/s4.jpg" alt=""></a>
                          <div class="list-button">
                             <a href="#"> <i class="fa fa-line-chart"> </i></a>
                             <a href="#"> <i class="fa fa-search"> </i></a>
                             <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
                             <a href="#"> <i class="fa fa-heart"> </i></a>
                             <a href="#"> <i class="fa fa-exchange"> </i></a>
                          </div>
                          <h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
                          <div class="star text-center">
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star-half-empty"></span>
                          </div>
                          <div class="price text-center">
                            <div class="price-new">$50</div>
                            <div class="price-old">$54</div>
                          </div>
                      </li>
                      <li class="item-product">
                          <a href="#"><img src="assets/img/fashion/s5.png" alt=""></a>
                          <div class="list-button">
                             <a href="#"> <i class="fa fa-line-chart"> </i></a>
                             <a href="#"> <i class="fa fa-search"> </i></a>
                             <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
                             <a href="#"> <i class="fa fa-heart"> </i></a>
                             <a href="#"> <i class="fa fa-exchange"> </i></a>
                          </div>
                          <h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
                          <div class="star text-center">
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star-half-empty"></span>
                          </div>
                          <div class="price text-center">
                            <div class="price-new">$50</div>
                            <div class="price-old">$54</div>
                          </div>
                      </li>
                      <li class="item-product">
                          <a href="#"><img src="assets/img/fashion/s6.png" alt=""></a>
                          <div class="list-button">
                             <a href="#"> <i class="fa fa-line-chart"> </i></a>
                             <a href="#"> <i class="fa fa-search"> </i></a>
                             <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
                             <a href="#"> <i class="fa fa-heart"> </i></a>
                             <a href="#"> <i class="fa fa-exchange"> </i></a>
                          </div>
                          <h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
                          <div class="star text-center">
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star-half-empty"></span>
                          </div>
                          <div class="price text-center">
                            <div class="price-new">$50</div>
                            <div class="price-old">$54</div>
                          </div>
                      </li>
                  </ul>
              </div>

              <a href="javascript:void(0)" class="jcarousel-control-prev">&lsaquo;</a>
              <a href="javascript:void(0)" class="jcarousel-control-next">&rsaquo;</a>
          </div>
      </div>

      <div class="category-product list-client">
          <div id="carousel-category-client" class="carousel slide" data-ride="carousel">
              <div class="carousel-inner">
                  <div class="item active">
                      <ul class=" row list-inline">
                          <li class="col-md-2 text-center">
                              <img src="assets/img/fashion/b1.jpg" alt="">
                          </li>
                          <li class="col-md-2 text-center">
                              <img src="assets/img/fashion/b2.png" alt="">
                          </li>
                          <li class="col-md-2 text-center">
                              <img src="assets/img/fashion/b3.jpg" alt="">
                          </li>
                          <li class="col-md-2 text-center">
                              <img src="assets/img/fashion/b4.jpg" alt="">
                          </li>
                          <li class="col-md-2 text-center">
                              <img src="assets/img/fashion/b5.jpg" alt="">
                          </li>
                          <li class="col-md-2 text-center">
                              <img src="assets/img/fashion/b6.jpg" alt="">
                          </li>
                          
                      </ul>
                  </div>

                   <div class="item ">
                      <ul class=" row list-inline">
                          <li class="col-md-2 text-center">
                              <img src="assets/img/fashion/b7.jpg" alt="">
                          </li>
                          <li class="col-md-2 text-center">
                              <img src="assets/img/fashion/b2.png" alt="">
                          </li>
                          <li class="col-md-2 text-center">
                              <img src="assets/img/fashion/b3.jpg" alt="">
                          </li>
                          <li class="col-md-2 text-center">
                              <img src="assets/img/fashion/b4.jpg" alt="">
                          </li>
                          <li class="col-md-2 text-center">
                              <img src="assets/img/fashion/b5.jpg" alt="">
                          </li>
                          <li class="col-md-2 text-center">
                              <img src="assets/img/fashion/b6.jpg" alt="">
                          </li>
                          
                      </ul>
                  </div>
                  
              </div>
              <!-- <a class="control-left carousel-control" href="#carousel-category-client" data-slide="prev"> <i class="fa fa-angle-left"></i></a>
              <a class=" control-right carousel-control" href="#carousel-category-client" data-slide="next"><i class="fa fa-angle-right"></i> </a> -->
          </div>
      </div>
  </div>