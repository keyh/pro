
<div class="container" id="columns">
    <h2 class="page-heading">
        <span class="page-heading-title2">Compare Products</span>
    </h2>
    <!-- ../page heading-->
    <div class="wrap-compare">
        
    </div>
    <div class="page-content">
        <table class="table table-bordered table-compare">
            <tr>
              
                <td>
                    <a href="#"><img src="assets/img/fashion/1.png" alt="Product"></a>
                </td>
                <td>
                    <a href="#"><img src="assets/img/fashion/1.png" alt="Product"></a>
                </td>
                <td>
                    <a href="#"><img src="assets/img/fashion/1.png" alt="Product"></a>
                </td>
                <td>
                    <a href="#"><img src="assets/img/fashion/1.png" alt="Product"></a>
                </td>
                <td>
                    <a href="#"><img src="assets/img/fashion/1.png" alt="Product"></a>
                </td>
                
            </tr>
            <tr>
                
                <td>
                    <a href="#">Perfomax</a>
                </td>
                <td>
                    <a href="#">Perfomax</a>
                </td>
                <td>
                    <a href="#">Perfomax</a>
                </td>
                <td>
                    <a href="#">Perfomax</a>
                </td>
                <td>
                    <a href="#">Perfomax</a>
                </td>
            </tr>
            <tr>
               
                <td>
                    <div class="product-star">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-o"></i>
                        <span>(3 Reviews)</span>
                    </div>
                </td>
                <td>
                    <div class="product-star">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-o"></i>
                        <span>(3 Reviews)</span>
                    </div>
                </td>
                <td>
                    <div class="product-star">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-o"></i>
                        <span>(3 Reviews)</span>
                    </div>
                </td>

                <td>
                    <div class="product-star">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-o"></i>
                        <span>(3 Reviews)</span>
                    </div>
                </td>
                <td>
                    <div class="product-star">
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star"></i>
                        <i class="fa fa-star-half-o"></i>
                        <span>(3 Reviews)</span>
                    </div>
                </td>
            </tr>
            <tr>
                
                <td class="price">$20</td>
                <td class="price">$21</td>
                <td class="price">$20.5</td>
                <td class="price">$21</td>
                <td class="price">$20.5</td>
            </tr>
            <tr>
                
                <td>Printed evening dress with straight sleeves with black thin waist belt and ruffled linings.</td>
                <td>Printed evening dress with straight sleeves with black thin waist belt and ruffled linings.</td>
                <td>Printed evening dress with straight sleeves with black thin waist belt and ruffled linings.</td>

                <td>Printed evening dress with straight sleeves with black thin waist belt and ruffled linings.</td>
                <td>Printed evening dress with straight sleeves with black thin waist belt and ruffled linings.</td>
            </tr>
            <tr>
               
                <td>CUCGU</td>
                <td>Nakia</td>
                <td>Gola</td>
                <td>Nakia</td>
                <td>Gola</td>
            </tr>
            <tr>
               
                <td class="instock">Instock (20 items)</td>
                <td class="outofstock">Out of stock</td>
                <td class="instock">Instock (20 items)</td>
                <td class="outofstock">Out of stock</td>
                <td class="instock">Instock (20 items)</td>
            </tr>
            <tr>
               
                <td>#873289</td>
                <td>#874389</td>
                <td>#874389</td>
                <td>#874389</td>
                <td>#874389</td>
            </tr>
            <tr>
               
                <td>X</td>
                <td>XL</td>
                <td>XS</td>
                <td>XL</td>
                <td>XS</td>
            </tr>
            <tr>
                
                <td>Blue</td>
                <td>Blue</td>
                <td>Blue</td>
                <td>Blue</td>
                <td>Blue</td>
            </tr>
            <tr>
                
                <td>0.3kg</td>
                <td>0.3kg</td>
                <td>0.3kg</td>
                <td>0.3kg</td>
                <td>0.3kg</td>
            </tr>
            <tr>
                
                <td>40x20x72cm</td>
                <td>40x20x72cm</td>
                <td>40x20x72cm</td>
                <td>40x20x72cm</td>
                <td>40x20x72cm</td>
            </tr>
            <tr>
               
                <td class="action">
                    <button class="add-cart btn btn-primary">Add to cart</button>
                    <button class="btn btn-primary"><span class="fa fa-heart-o"></span></button>
                    <button class="btn btn-primary">&times</button>
                </td>
                <td class="action">
                    <button class="add-cart button btn btn-primary">Add to cart</button>
                    <button class="btn btn-primary"><span class="fa fa-heart-o"></span></button>
                    <button class="btn btn-primary">&times</button>
                </td>
                <td class="action">
                    <button class="add-cart btn btn-primary">Add to cart</button>
                    <button class="btn btn-primary"><span class="fa fa-heart-o"></span></button>
                    <button class="btn btn-primary">&times</button>
                </td>

                <td class="action">
                    <button class="add-cart btn btn-primary">Add to cart</button>
                    <button class="btn btn-primary"><span class="fa fa-heart-o"></span></button>
                    <button class="btn btn-primary">&times</button>
                </td>
                <td class="action">
                    <button class="add-cart btn btn-primary">Add to cart</button>
                    <button class="btn btn-primary"><span class="fa fa-heart-o"></span></button>
                    <button class="btn btn-primary">&times</button>
                </td>
            </tr>
        </table>
    </div>
</div>

