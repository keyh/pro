<div class="container">
	<div class="single-product">
		<div class="media">
			<div class="pull-left col-md-4">
				
		      <div id="carousel-single-product" class="carousel slide" data-ride="carousel">
		          <div class="carousel-inner">
		              <div class="item active">
							<img class="media-object" src="assets/img/fashion/1.png" alt="Image">		                 
						</div>
		              <div class="item">
							<img class="media-object" src="assets/img/fashion/2.png" alt="Image">		                   
		              </div>
		              <div class="item">
							<img class="media-object" src="assets/img/fashion/3.png" alt="Image">		                 
		              </div>
		              <div class="item">
							<img class="media-object" src="assets/img/fashion/4.png" alt="Image">		                 
		              </div>
		              <div class="item">
							<img class="media-object" src="assets/img/fashion/5.png" alt="Image">		                 
		              </div>
		              <div class="item">
							<img class="media-object" src="assets/img/fashion/6.png" alt="Image">		                 
		              </div>
		          </div>
					<a class="left carousel-control" href="#carousel-single-product" data-slide="prev"> <i class="fa fa-long-arrow-left"></i></a>		
					<a class="right carousel-control" href="#carousel-single-product" data-slide="next"> <i class="fa fa-long-arrow-right"></i> </a>
		          <div class="list-item-control">
		              <div data-target="#carousel-single-product" data-slide-to="0" class=" active item-control">
		              		<img class="media-object" src="assets/img/fashion/1.png" alt="Image">
		              </div>
		              <div data-target="#carousel-single-product" data-slide-to="1" class="item-control">
		              		<img class="media-object" src="assets/img/fashion/2.png" alt="Image">
		              </div>
		              <div data-target="#carousel-single-product" data-slide-to="2" class="item-control">
		              		<img class="media-object" src="assets/img/fashion/3.png" alt="Image">
		              </div>
		              <div data-target="#carousel-single-product" data-slide-to="3" class="item-control">
		              		<img class="media-object" src="assets/img/fashion/4.png" alt="Image">
		              </div>
		              <div data-target="#carousel-single-product" data-slide-to="4" class="item-control">
		              		<img class="media-object" src="assets/img/fashion/5.png" alt="Image">
		              </div>
		              <div data-target="#carousel-single-product" data-slide-to="5" class="item-control">
		              		<img class="media-object" src="assets/img/fashion/6.png" alt="Image">
		              </div>
		          </div>
		      </div>
			</div>
			<div class="media-body">
				<h2 class="media-heading">Gack Pocket Square</h2>
				<div class="social-list">
					<ul class="list-inline">
						<li class="btn btn-default btn-sm sc-twitter"><i class="fa fa-twitter"></i> Twitter</li>
						<li class="btn btn-default btn-sm sc-facebook"><i class="fa fa-facebook"></i> Face</li>
						<li class="btn btn-default btn-sm sc-google"> <i class="fa fa-google-plus"></i> Google</li>
					</ul>
				</div>
				<div class="star">
        			<span class="fa fa-star"></span>
        			<span class="fa fa-star"></span>
        			<span class="fa fa-star"></span>
        			<span class="fa fa-star"></span>
        			<span class="fa fa-star-half-empty"></span>
        		</div>
        		<div class="label-product padding-md-180">
        			<p><strong>Nhãn hiệu:</strong> <span>Samsung</span></p>
        			<p><strong>Địa chỉ: </strong> <span>Lương sơn - Hòa Bình</span></p>
        			<p><strong>SDT: </strong> <span>0989 342 070</span></p>
	    		</div>
        		<div class="price">
        			<span class="price-old">$20</span>
        			<span class="price-new">$15</span>
        		</div>
        		<div class="list-btn padding-md-180">
					<ul class="list-inline">
	    				<li>
	    					<form action="" method="POST" class="form-inline" role="form">
	    						<div class="form-group">
	    							<input type="text" class="form-control" value="1">
	    						</div>
	    					</form>
	    				</li><li>
	    					<a href="#" class="btn btn-primary add-to-cart"> Add to cart</a>
	    				</li>
	    				<li>
	    					<a href="#" class="btn btn-default top-product"> <i class="fa fa-line-chart"></i></a>
	    				</li>
	    				<li><a href="#" class="btn btn-default wishlist"><i class="fa fa-heart-o"></i></a></li>
	    				<li><a href="#" class="btn btn-default compare"><i class="fa fa-exchange"></i></a></li>
	    			</ul>
	    		</div>
	    		
			</div>
		</div>
		<div role="tabpanel" class="tab-info-product">
		    <!-- Nav tabs -->
		    <ul class="nav nav-tabs" role="tablist">
		        <li role="presentation" class="active">
		            <a href="#detail-product" aria-controls="home" role="tab" data-toggle="tab">Chi tiếp sản phẩm</a>
		        </li>
		        <li role="presentation">
		            <a href="#info-product" aria-controls="tab" role="tab" data-toggle="tab">Thông tin sản phẩm</a>
		        </li>
		        <li role="presentation">
		            <a href="#review-product" aria-controls="tab" role="tab" data-toggle="tab">Đánh giá sản phẩm</a>
		        </li>
		    </ul>
		
		    <!-- Tab panes -->
		    <div class="tab-content tab-content-info">
		        <div role="tabpanel" class="tab-pane active" id="detail-product">
		        	Fashion has been creating well-designed collections since 2010. The brand offers feminine designs delivering stylish separates and statement dresses which have since evolved into a full ready-to-wear collection in which every item is a vital part of a woman's wardrobe. The result? Cool, easy, chic looks with youthful elegance and unmistakable signature style. All the beautiful pieces are made in Italy and manufactured with the greatest attention. Now Fashion extends to a range of accessories including shoes, hats, belts and more!

		        </div>
		        <div role="tabpanel" class="tab-pane" id="info-product">
		        	<table class="table table-bordered table-striped table-hover">
					    <thead>
					      <tr>
					        <th>#</th>
					        <th>First Name</th>
					        <th>Last Name</th>
					        <th>Username</th>
					      </tr>
					    </thead>
					    <tbody>
					      <tr>
					        <td>1</td>
					        <td>Mark</td>
					        <td>Otto</td>
					        <td>@mdo</td>
					      </tr>
					      <tr>
					        <td>2</td>
					        <td>Jacob</td>
					        <td>Thornton</td>
					        <td>@fat</td>
					      </tr>
					      <tr>
					        <td>3</td>
					        <td>Larry</td>
					        <td>the Bird</td>
					        <td>@twitter</td>
					      </tr>
					    </tbody>
					</table>

		        </div>
		        <div role="tabpanel" class="tab-pane" id="review-product">
		        	<div class="row">
		        		<div class="col-md-6">
		        			<div class="media">
		        				<div class="pull-left">
		        					<p>Đánh giá của bạn?</p>
		        				</div>
		        				<div class="media-body">
		        					<div class="star">
				            		zr	<span class="fa fa-star"></span>
				            			<span class="fa fa-star"></span>
				            			<span class="fa fa-star"></span>
				            			<span class="fa fa-star"></span>
				            			<span class="fa fa-star"></span>
				            		</div>
				            		<div class="star">
				            			<span class="fa fa-star"></span>
				            			<span class="fa fa-star"></span>
				            			<span class="fa fa-star"></span>
				            			<span class="fa fa-star"></span>
				            			
				            		</div>
				            		<div class="star">
				            			<span class="fa fa-star"></span>
				            			<span class="fa fa-star"></span>
				            			<span class="fa fa-star"></span>
				            			
				            		</div>
				            		<div class="star">
				            			<span class="fa fa-star"></span>
				            			<span class="fa fa-star"></span>
				            			
				            		</div>
				            		<div class="star">
				            			<span class="fa fa-star"></span>
				            			
				            		</div>
		        				</div>
		        			</div>
		        		</div>
		        		<div class="col-md-6">
		        			<form action="" method="POST" role="form">
		        				
		        			
		        				<div class="form-group">
		        					<label for="">label</label>
		        					<input type="text" class="form-control" id="" placeholder="Input field">
		        				</div>
		        				<div class="control-group">
						            <label class="control-label" for="textarea">Textarea</label>
						            <div class="controls">
						              <textarea class="form-control input-xlarge" id="textarea" rows="3"></textarea>
						            </div>
						        </div>
						        <div class="form-group rating">
						        	
						        	<p>
						        		<span>Xấu</span>
							        	<input type="radio">
							        	<input type="radio">
							        	<input type="radio">
							        	<input type="radio">
							        	<input type="radio">
							        	<span>Tốt</span>
							        </p>
							        
						        </div>
						        <div class="form-group">
		        					<label for="">label</label>
		        					<input type="text" class="form-control" id="" placeholder="Input field">
		        				</div>
		        			
		        				<button type="submit" class="btn btn-primary pull-right">Submit</button>
		        			</form>
		        		</div>
		        	</div>
		        </div>
		    </div>
		</div>
		      <div class="row category-product">
          <div class="title-category">
              <a href="#"> <span>Sản phẩm cùng loại</span> </a>
          </div>
          <div class="jcarousel-wrapper">
              <div class="jcarousel">
                  <ul class="list-item-product">
                      <li>
                          <a href="#"><img src="assets/img/fashion/1.png" alt="Image 1"></a>
                          <div class="list-button">
                             <a href="#"> <i class="fa fa-line-chart"> </i></a>
                             <a href="#"> <i class="fa fa-search"> </i></a>
                             <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
                             <a href="#"> <i class="fa fa-heart"> </i></a>
                             <a href="#"> <i class="fa fa-exchange"> </i></a>
                          </div>
                      </li>
                      <li>
                          <a href="#"><img src="assets/img/fashion/2.png" alt="Image 1"></a>
                          <div class="list-button">
                             <a href="#"> <i class="fa fa-line-chart"> </i></a>
                             <a href="#"> <i class="fa fa-search"> </i></a>
                             <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
                             <a href="#"> <i class="fa fa-heart"> </i></a>
                             <a href="#"> <i class="fa fa-exchange"> </i></a>
                          </div>
                      </li>
                      <li>
                          <a href="#"><img src="assets/img/fashion/3.png" alt="Image 1"></a>
                          <div class="list-button">
                             <a href="#"> <i class="fa fa-line-chart"> </i></a>
                             <a href="#"> <i class="fa fa-search"> </i></a>
                             <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
                             <a href="#"> <i class="fa fa-heart"> </i></a>
                             <a href="#"> <i class="fa fa-exchange"> </i></a>
                          </div>
                      </li>
                      <li>
                          <a href="#"><img src="assets/img/fashion/4.png" alt="Image 1"></a>
                          <div class="list-button">
                             <a href="#"> <i class="fa fa-line-chart"> </i></a>
                             <a href="#"> <i class="fa fa-search"> </i></a>
                             <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
                             <a href="#"> <i class="fa fa-heart"> </i></a>
                             <a href="#"> <i class="fa fa-exchange"> </i></a>
                          </div>
                      </li>
                      <li>
                          <a href="#"><img src="assets/img/fashion/5.png" alt="Image 1"></a>
                          <div class="list-button">
                             <a href="#"> <i class="fa fa-line-chart"> </i></a>
                             <a href="#"> <i class="fa fa-search"> </i></a>
                             <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
                             <a href="#"> <i class="fa fa-heart"> </i></a>
                             <a href="#"> <i class="fa fa-exchange"> </i></a>
                          </div>
                      </li>
                      <li>
                          <a href="#"><img src="assets/img/fashion/6.png" alt="Image 1"></a>
                          <div class="list-button">
                             <a href="#"> <i class="fa fa-line-chart"> </i></a>
                             <a href="#"> <i class="fa fa-search"> </i></a>
                             <a href="#"> <i class="fa fa-shopping-cart"> </i></a>
                             <a href="#"> <i class="fa fa-heart"> </i></a>
                             <a href="#"> <i class="fa fa-exchange"> </i></a>
                          </div>
                      </li>
                  </ul>
              </div>

              <a href="#" class="jcarousel-control-prev">&lsaquo;</a>
              <a href="#" class="jcarousel-control-next">&rsaquo;</a>
          </div>
      </div>
	</div>
</div>