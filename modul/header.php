<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Title Page</title>

        <link rel="stylesheet" type="text/css" href="assets/css/lily-store.css">
       
        <script src="assets/vendor/jquery/dist/jquery.js" type="text/javascript" charset="utf-8" async defer></script>
        <script type="text/javascript" src="assets/vendor/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="assets/js/jquery.jcarousel.min.js"></script>
        <script src="assets/js/masonry.js"></script>
        <script src="assets/js/main.js"></script>
	</head>
	<body>
		<div class="header-top bg-default">
			<div class="container">
				<div class="pull-left">
					<ul class="list-inline">
						<li>Free shipping</li>
						<li>Phone: 1398 7847 976 </li>
						<li>Hot line: 8762 908 865</li>
					</ul>
				</div>
				<div class="pull-right list-right">
					<ul>
						<li><a href="#"><i class="fa fa-user"></i></a></li>
						<li><a href="#">Kiểm tra giỏ hàng</a></li>
						<li><a href="#">Sản phẩm yêu thích</a></li>
						<li><a href="#">Tài khoản</a></li>
						<li><a href="#">Giỏ hàng</a></li>
						<li><a href="#modal-id" data-toggle="modal">Đăng nhập/ Đăng ký</a></li>
					</ul>
				</div>
			</div>
		</div>
		<div class="header-bar bg-gray">
			<div class="container">
				<div class="row">
					<div class="col-md-4 hide-sm">
						<img src="assets/img/logo-store.png" alt="">
					</div>
					<div class="col-md-8">
						<div class="search-form">
							
							<form action="" method="POST" class="form-horizontal" role="form">
									<div class="wrap-input">
								      <input type="text" class="form-control" placeholder="Search for...">
								      <span class="input-group-btn btn-search">
								        <button class="btn btn-default" type="button"><i class="fa fa-search"></i></button>
								      </span>
								    </div>
							</form>
							<div class="contact_block hide-sm">
								<span>0123-456-789</span>
							</div>
						</div>
					</div>
				</div>
				
				
			</div>
		</div>
		<div class="menu-bar">
			<div class="container po-relative">
				<nav class="navbar navbar-default mega-menu" role="navigation">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
							<span class="sr-only">Toggle navigation</span>
							<span class="fa fa-bars"></span>
						</button>
						<a class="navbar-brand active" href="#">Home</a>
					</div>
				
					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse navbar-ex1-collapse ">
						<ul class="nav navbar-nav parent-menu">
							<li>
								<a href="#">Women</a>
								<ul class="list-unstyled child-menu">
									<li><a href="#">Shose</a></li>
									<li><a href="#">T-shirt</a></li>
									<li><a href="#">Hats</a></li>
								</ul>
							</li>
							<li><a href="#">Men</a></li>
							<li><a href="#">Clothing</a></li>
							<li><a href="#">T-shirts</a></li>
							<li><a href="#">About Us</a></li>
							<li><a href="#">Contact</a></li>
						</ul>
						
					</div><!-- /.navbar-collapse -->
				</nav>
				<div class="div-cart po-absolute">
					<ul class="pull-right list-inline menu-right">
			            <li class="my-cart"><a href="#"><i class="fa fa-shopping-cart "></i> Giỏ Hàng</a></li>
			            <li class="number-item"><a href="#"><span>0</span></a></li>
					</ul>
				</div>
			</div>
		</div>