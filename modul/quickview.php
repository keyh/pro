<!-- Modal Quickview -->
<div class="modal fade category-product " id="modal-quickview">
<div class="modal-dialog modal-lg">
	<div class="modal-content ">
		<button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">
			<img src="assets/img/btn-close.png" alt="">
		</button>
		
		<div class="modal-body quickview-product">
			<div class="media">
				<div class="pull-left">
					<div id="carousel-product" class="carousel slide" data-ride="carousel">
					    
					    <div class="carousel-inner">
					        <div class="item active">
					        	<a href="#"><img src="assets/img/fashion/1.png" alt=""></a>
					        </div>
					        <div class="item">
					        	<a href="#"><img src="assets/img/fashion/2.png" alt=""></a>
					        </div>
					        <div class="item ">
					        	<a href="#"><img src="assets/img/fashion/3.png" alt=""></a>
					        </div>
					    </div>
					    <a class="control-left-qv carousel-control" href="#carousel-product" data-slide="prev"><i class="fa fa-angle-left"></i></a>
					    <a class="control-right-qv carousel-control" href="#carousel-product" data-slide="next"><i class="fa fa-angle-right"></i></a>
					</div>
				</div>
				<div class="media-body info-product">
					<h2 class="media-heading">Huma saren mazem kae</h2>
					<p class="description">Fusce porttitor at ante eu egestas. Morbi vulputate diam at nibh imperdiet pretium. Nulla euismod, nibh nec tincidunt maximus, elit sem ornare nunc, et dictum elit dui sed justo. Donec scelerisque, erat vel pharetra luctus, nibh tortor efficitur nibh, non euismod leo diam ut risus.</p>
					
            		<div class="star">
            			<span class="fa fa-star"></span>
            			<span class="fa fa-star"></span>
            			<span class="fa fa-star"></span>
            			<span class="fa fa-star"></span>
            			<span class="fa fa-star-half-empty"></span>
            		</div>
            		<div class="label-product padding-md-180">
	        			<p><strong>Nhãn hiệu:</strong> <span>Samsung</span></p>
	        			<p><strong>Địa chỉ: </strong> <span>Lương sơn - Hòa Bình</span></p>
	        			<p><strong>SDT: </strong> <span>0989 342 070</span></p>
		    		</div>
            		<div class="price">
            			<span class="price-old">$20</span>
            			<span class="price-new">$15</span>
            		</div>
            		<div class="list-btn">
            			<ul class="list-inline">
            				<li>
            					<form action="" method="POST" class="form-inline" role="form">
            						<div class="form-group">
            							<input type="text" class="form-control" value="1">
            						</div>
            					</form>
            				<li>
            					<a href="#" class="btn btn-primary add-to-cart"> Add to cart</a>
            				</li>
            				<li>
            					<a href="#" class="btn btn-default top-product"> <i class="fa fa-line-chart"></i></a>
            				</li>
            				<li><a href="#" class="btn btn-default wishlist"><i class="fa fa-heart-o"></i></a></li>
            				<li><a href="#" class="btn btn-default compare"><i class="fa fa-exchange"></i></a></li>

            			</ul>
            		</div>
				</div>
			</div>
		</div>
		
	</div>
</div>
</div>