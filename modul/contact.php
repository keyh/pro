<div class="page-contact">
<!-- 	<div class="embed-responsive embed-responsive-16by9 map-contact ">
	  <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d8856.075937987378!2d105.74031878486423!3d21.051684385586512!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x313454581d1756dd%3A0xa95478cfe6f9fb48!2zMjAsIDI5LCAzMiwgNzAsIDcz!5e0!3m2!1sen!2s!4v1437707489245" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
	</div> -->

	<div class="container">
		<div class="row">
			<div class="col-md-7">
				<form action="" method="POST" role="form">			
					<div class="form-group">
						<label for="" class="po-relative">Họ tên <span class="require">*</span></label>
						<input type="text" class="form-control" id="" placeholder="Họ tên">
					</div>
					<div class="form-group">
						<label for="" class="po-relative">Địa chỉ <span class="require">*</span></label>
						<input type="text" class="form-control" id="" placeholder="Địa chỉ">
					</div>

					<div class="form-group">
						<label for="" class="po-relative">Email <span class="require">*</span></label>
						<input type="text" class="form-control" id="" placeholder="Email">
					</div>

					<div class="form-group">
						<label for="" class="po-relative">Số điện thoại <span class="require">*</span></label>
						<input type="text" class="form-control" id="" placeholder="Số điện thoại">
					</div>

					<div class="form-group">
						<label for="" class="po-relative">Tiêu đề <span class="require">*</span></label>
						<input type="text" class="form-control" id="" placeholder="Tiêu đề">
					</div>

					<div class="form-group">
						<label for="" class="po-relative">Nội dung <span class="require">*</span></label>
						<textarea class="form-control" id="" placeholder="Nội dung"></textarea>
					</div>
					
					<div class="form-group row">
						<div class="col-md-7">
							<label for="" class="po-relative">Mã xác nhận <span class="require">*</span></label>
							<input type="text" class="form-control">

						</div>
						<div class="col-md-5 code-captra">
							<img src="assets/img/captra.png" class="pull-left" width="100" alt="">
							<a href="#" class="btn btn-primary"><i class="fa fa-refresh"></i></a>
						</div>
					</div>
				
					<button type="submit" class="btn btn-primary">Gửi</button>
				</form>
			</div>
			<div class="col-md-5">
				<div class="embed-responsive embed-responsive-16by9 map-contact ">
				  <iframe class="embed-responsive-item" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d8856.075937987378!2d105.74031878486423!3d21.051684385586512!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x313454581d1756dd%3A0xa95478cfe6f9fb48!2zMjAsIDI5LCAzMiwgNzAsIDcz!5e0!3m2!1sen!2s!4v1437707489245" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
				</div>
				<div class="info-contact">
					<p><strong>Địa chỉ:</strong> Từ Cầu  - Hà nội </p>
					<p><strong>Điện thoại:</strong> 0989 342 070</p>
					<p><strong>Fax:</strong> 1245 76538 </p>
					<p><strong>Email:</strong><a href=""> contact@gmail.com</a> </p>
					<p><strong>Website:</strong><a href="https://www.google.com/"> address</a></p>

				</div>
			</div>
		</div>
	</div>
</div>