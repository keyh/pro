<div class="my-account">
	<div class="container">
		<div class="entry-info">
			<div class="media">
				<a class="pull-left" href="#">
					<img class="media-object avatar img-circle" src="https://placehold.it/100x100" alt="Image">
				</a>
				<div class="media-body">
					<h4 class="media-heading">Nguyễn Thị Huệ</h4>
					<p>nguyenthihue.cntt@gmail.com</p>
					<p>0989342070</p>
					<p></p>

				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3 list-info">
				<ul class="list-unstyled">
					<Li><a class="active" href="#info-personal">Thông tin cá nhân</a></Li>
					<Li><a href="#info-product">Sản phẩm của tôi</a></Li>
					<Li><a href="#add-product">Thêm mới sản phẩm</a></Li>
				</ul>
			</div>		
			<div class="col-md-9">
				<div id="info-personal" class="box-container">
					<div class="panel panel-default">
						  <div class="panel-heading">
								<h3 class="panel-title"><i class="fa fa-phone"></i>Thông tin cá nhân</h3>
						  </div>
						  <div class="panel-body">
								<form class="form-horizontal">
								<fieldset>
								
								<div class="form-group">
								  <label for="first-name" class="col-lg-2 control-label">Tên</label>
								  <div class="col-lg-10">
								    <input type="text" class="form-control" id="first-name" placeholder="Tên">
								  </div>
								</div>

								<div class="form-group">
								  <label for="last-name" class="col-lg-2 control-label">Họ</label>
								  <div class="col-lg-10">
								    <input type="text" class="form-control" id="last-name" placeholder="Họ">
								  </div>
								</div>

								<div class="form-group">
								  <label for="telephone" class="col-lg-2 control-label">Số điện thoại</label>
								  <div class="col-lg-10">
								    <input type="text" class="form-control" id="telephone" placeholder="Số điện thoại">
								  </div>
								</div>


								<div class="form-group">
								  <label for="inputEmail" class="col-lg-2 control-label">Email</label>
								  <div class="col-lg-10">
								    <input type="text" class="form-control" id="inputEmail" placeholder="Email">
								  </div>
								</div>


								<div class="form-group">
								  <label for="inputPassword" class="col-lg-2 control-label">Mật khẩu</label>
								  <div class="col-lg-10">
								    <input type="password" class="form-control" id="inputPassword" placeholder="Mật khẩu">   
								  </div>
								</div>

								<div class="form-group">
								  <label for="inputPassword-conf" class="col-lg-2 control-label">Nhập lại mật khẩu</label>
								  <div class="col-lg-10">
								    <input type="password" class="form-control" id="inputPassword-conf" placeholder="Nhập lại ật khẩu">   
								  </div>
								</div>
								<div class="row">
									<div class="col-md-6">
										<div class="form-group">
										  <label class="col-lg-4 control-label">Giới tính</label>
										  <div class="col-lg-8">
										    <div class="radio">
										      <label><input type="radio" name="gender" id="gender1" value="option1" checked=""> Name</label>
										    </div>
										    <div class="radio">
										      <label><input type="radio" name="gender" id="gender2" value="option2"> Nữ </label>
										    </div>
										  </div>
										</div>	
									</div>
									<div class="col-md-6">
										<div class="form-group">
										  <label class="col-lg-6 control-label">Theo dõi bản tin</label>
										  <div class="col-lg-6">
										    <div class="radio">
										      <label><input type="radio" name="subcribe" id="subcribe1" value="option1" checked=""> Subcribe</label>
										    </div>
										    <div class="radio">
										      <label><input type="radio" name="subcribe" id="subcribe2" value="option2">Unsubcribe</label>
										    </div>
										  </div>
										</div>								
									</div>
								</div>

								<div class="form-group">
								  <div class="col-lg-10 col-lg-offset-2">
								    <button type="submit" class="btn btn-primary pull-right btn-submit">Submit</button>
								    <button type="reset" class="btn btn-default pull-right">Cancel</button>
								  
								  </div>
								</div>
								</fieldset>
								</form>
						  </div>
					</div>
				</div>
				<div id="info-product" class="box-container">
					<div class="panel panel-default">
						  <div class="panel-heading">
								<h3 class="panel-title"><i class="fa fa-book"></i>Sản phẩm của tôi</h3>
						  </div>
						  <div class="panel-body">
								<div class="row">
									<ul class="list-inline">
										<li class=" col-md-4 item-product">
                          					<a href="#"><img src="assets/img/fashion/1.png" alt="Image 1"></a>
					                        <h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
					                        <div class="star text-center">
					                            <span class="fa fa-star"></span>
					                            <span class="fa fa-star"></span>
					                            <span class="fa fa-star"></span>
					                            <span class="fa fa-star"></span>
					                            <span class="fa fa-star-half-empty"></span>
					                        </div>
					                        <div class="price text-center">
					                            <div class="price-new">$50</div>
					                            <div class="price-old">$54</div>
				                          	</div>
				                          	<div class="btn-setting btn btn-primary">
				                          		<i class="fa fa-cog"></i>
				                          		<div class="action">
				                          			<ul class="list-unstyled">
				                          				<li> <a href="#"><i class="fa fa fa-search"></i> Xem</a> </li>
				                          				<li> <a href="#"><i class=" fa fa-times"></i> Xóa</a> </li>
				                          				<li> <a href="#"> <i class=" fa fa-pencil-square-o "></i> Sửa</a> </li>
				                          			</ul>
				                          		</div>
				                          	</div>
				                        </li>
										<li class=" col-md-4 item-product">
                          					<a href="#"><img src="assets/img/fashion/1.png" alt="Image 1"></a>
					                        <h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
					                        <div class="star text-center">
					                            <span class="fa fa-star"></span>
					                            <span class="fa fa-star"></span>
					                            <span class="fa fa-star"></span>
					                            <span class="fa fa-star"></span>
					                            <span class="fa fa-star-half-empty"></span>
					                        </div>
					                        <div class="price text-center">
					                            <div class="price-new">$50</div>
					                            <div class="price-old">$54</div>
				                          	</div>
				                          	<div class="btn-setting btn btn-primary">
				                          		<i class="fa fa-cog"></i>
				                          		<div class="action">
				                          			<ul class="list-unstyled">
				                          				<li> <a href="#"><i class="fa fa fa-search"></i> Xem</a> </li>
				                          				<li> <a href="#"><i class=" fa fa-times"></i> Xóa</a> </li>
				                          				<li> <a href="#"> <i class=" fa fa-pencil-square-o "></i> Sửa</a> </li>
				                          			</ul>
				                          		</div>
				                          	</div>
				                        </li>
										<li class=" col-md-4 item-product">
                          					<a href="#"><img src="assets/img/fashion/1.png" alt="Image 1"></a>
					                        <h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
					                        <div class="star text-center">
					                            <span class="fa fa-star"></span>
					                            <span class="fa fa-star"></span>
					                            <span class="fa fa-star"></span>
					                            <span class="fa fa-star"></span>
					                            <span class="fa fa-star-half-empty"></span>
					                        </div>
					                        <div class="price text-center">
					                            <div class="price-new">$50</div>
					                            <div class="price-old">$54</div>
				                          	</div>
				                          	<div class="btn-setting btn btn-primary">
				                          		<i class="fa fa-cog"></i>
				                          		<div class="action">
				                          			<ul class="list-unstyled">
				                          				<li> <a href="#"><i class="fa fa fa-search"></i> Xem</a> </li>
				                          				<li> <a href="#"><i class=" fa fa-times"></i> Xóa</a> </li>
				                          				<li> <a href="#"> <i class=" fa fa-pencil-square-o "></i> Sửa</a> </li>
				                          			</ul>
				                          		</div>
				                          	</div>
				                        </li>
										<li class=" col-md-4 item-product">
                          					<a href="#"><img src="assets/img/fashion/1.png" alt="Image 1"></a>
					                        <h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
					                        <div class="star text-center">
					                            <span class="fa fa-star"></span>
					                            <span class="fa fa-star"></span>
					                            <span class="fa fa-star"></span>
					                            <span class="fa fa-star"></span>
					                            <span class="fa fa-star-half-empty"></span>
					                        </div>
					                        <div class="price text-center">
					                            <div class="price-new">$50</div>
					                            <div class="price-old">$54</div>
				                          	</div>
				                          	<div class="btn-setting btn btn-primary">
				                          		<i class="fa fa-cog"></i>
				                          		<div class="action">
				                          			<ul class="list-unstyled">
				                          				<li> <a href="#"><i class="fa fa fa-search"></i> Xem</a> </li>
				                          				<li> <a href="#"><i class=" fa fa-times"></i> Xóa</a> </li>
				                          				<li> <a href="#"> <i class=" fa fa-pencil-square-o "></i> Sửa</a> </li>
				                          			</ul>
				                          		</div>
				                          	</div>
				                        </li>
										<li class=" col-md-4 item-product">
                          					<a href="#"><img src="assets/img/fashion/1.png" alt="Image 1"></a>
					                        <h3 class="title-product text-center"><a href="#">Gack Pocket Square</a></h3>
					                        <div class="star text-center">
					                            <span class="fa fa-star"></span>
					                            <span class="fa fa-star"></span>
					                            <span class="fa fa-star"></span>
					                            <span class="fa fa-star"></span>
					                            <span class="fa fa-star-half-empty"></span>
					                        </div>
					                        <div class="price text-center">
					                            <div class="price-new">$50</div>
					                            <div class="price-old">$54</div>
				                          	</div>
				                          	<div class="btn-setting btn btn-primary">
				                          		<i class="fa fa-cog"></i>
				                          		<div class="action">
				                          			<ul class="list-unstyled">
				                          				<li> <a href="#"><i class="fa fa fa-search"></i> Xem</a> </li>
				                          				<li> <a href="#"><i class=" fa fa-times"></i> Xóa</a> </li>
				                          				<li> <a href="#"> <i class=" fa fa-pencil-square-o "></i> Sửa</a> </li>
				                          			</ul>
				                          		</div>
				                          	</div>
				                        </li>

									</ul>
								</div>
						  </div>
					</div>
				</div>
				<div id="add-product" class="box-container">
					<div class="panel panel-default">
						  <div class="panel-heading">
								<h3 class="panel-title"> <i class="fa fa-puzzle-piece"></i>Thêm sản phẩm</h3>
						  </div>
						  <div class="panel-body">
								
						  </div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>