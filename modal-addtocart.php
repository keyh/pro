
<div class="modal fade" id="modal-addtocart">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			
			<button type="button" class="modal-close" data-dismiss="modal" aria-hidden="true">
				<img src="assets/img/btn-close.png" alt="">
			</button>
			<div class="modal-body">
			    <table class="table view-cart">
			        <thead>
			            <tr>
			                <th></th>
			                <th></th>
			                <th>Tên sản phẩm</th>
			                <th>Giá sản phẩm</th>
			                <th>Số lượng</th>
			                <th>Tổng</th>
			            </tr>
			        </thead>
			        <tbody>
			            <tr>
			                <td><a href="#" class="remove-product"><i class="fa fa-times"></i></a></td>
			                <td><a href="#"><img src="assets/img/fashion/1.png" alt=""></a></td>
			                <td><a href="#" class="name-product">this is name fashion 1</a></td>
			                <td>$40</td>
			                <td><input type="number" class="quantity" value="2"> <a href="#" class="btn btn-primary"><i class="fa fa-refresh"></i></a> </td>
			                <td>$80</td>
			            </tr>
			            <tr>
			                <td><a href="#" class="remove-product"><i class="fa fa-times"></i></a></td>
			                <td><a href="#"><img src="assets/img/fashion/1.png" alt=""></a></td>
			                <td><a href="#" class="name-product">this is name fashion 1</a></td>
			                <td>$40</td>
			                <td><input type="number" class="quantity" value="2"> <a href="#" class="btn btn-primary"><i class="fa fa-refresh"></i></a></td>
			                <td>$80</td>
			            </tr>
			            <tr>
			                <td><a href="#" class="remove-product"><i class="fa fa-times"></i></a></td>
			                <td><a href="#"><img src="assets/img/fashion/1.png" alt=""></a></td>
			                <td><a href="#" class="name-product">this is name fashion 1</a></td>
			                <td>$40</td>
			                <td><input type="number" class="quantity" value="2"> <a href="#" class="btn btn-primary"><i class="fa fa-refresh"></i></a></td>
			                <td>$80</td>
			            </tr>
			            <tr>
			                <td><a href="#" class="remove-product"><i class="fa fa-times"></i></a></td>
			                <td><a href="#"><img src="assets/img/fashion/1.png" alt=""></a></td>
			                <td><a href="#" class="name-product">this is name fashion 1</a></td>
			                <td>$40</td>
			                <td><input type="number" class="quantity" value="2"> <a href="#" class="btn btn-primary"><i class="fa fa-refresh"></i></a></td>
			                <td>$80</td>
			            </tr>
			            <tr>
			                <td><a href="#" class="remove-product"><i class="fa fa-times"></i></a></td>
			                <td><a href="#"><img src="assets/img/fashion/1.png" alt=""></a></td>
			                <td><a href="#" class="name-product">this is name fashion 1</a></td>
			                <td>$40</td>
			                <td><input type="number" class="quantity" value="2"> <a href="#" class="btn btn-primary"><i class="fa fa-refresh"></i></a></td>
			                <td>$80</td>
			            </tr>
			            <tr>
			                <td><a href="#" class="remove-product"><i class="fa fa-times"></i></a></td>
			                <td><a href="#"><img src="assets/img/fashion/1.png" alt=""></a></td>
			                <td><a href="#" class="name-product">this is name fashion 1</a></td>
			                <td>$40</td>
			                <td><input type="number" class="quantity" value="2"> <a href="#" class="btn btn-primary"><i class="fa fa-refresh"></i></a></td>
			                <td>$80</td>
			            </tr>			            			            			            
			            <tr>
			                <td colspan="6">
			                    <div class="coupon pull-left">
			                        <input type="text" placeholder="Mã khuyến mãi">
			                        <div class="btn btn-default btn-coupon">Apply</div>
			                    </div>
								<div class="pull-right">
									<h4><strong>Tổng: </strong> $100</h4>
								</div>
			                </td>
			                
			            </tr>
			        </tbody>
			    </table>
			    <div class="row">
			        <div class="col-md-6 col-md-offset-6">
			       		
			            <div class="check-out pull-right">
			            	
			                <a href="#" class="btn btn-primary">Thanh toán <i class="fa fa-arrow-circle-o-right"></i></a>
			            </div>
			        </div>
			    </div>
			</div>
		</div>
	</div>
</div>