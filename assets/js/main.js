jQuery(document).ready(function(){
	$('.my-cart, .number-item').click(function() {
		$('.mini-cart').addClass('active');
		$('.mark-black').addClass('active');
	});
	$('.close-mini-cart, .mark-black, .close-cat').click(function() {
		$('.mini-cart').removeClass('active');
		$('.mark-black').removeClass('active');
        $('.menu-cat').removeClass('active');
        $('.show-cat').removeClass('hide-cat');
	});
	
    
    $('.show-cat').click(function() {
        $(this).addClass('hide-cat');
        $('.menu-cat').addClass('active');
        $('.mark-black').addClass('active');
    });

	// carousel
	 $(function() {
        var jcarousel = $('.jcarousel');

        jcarousel
            .on('jcarousel:reload jcarousel:create', function () {
                var carousel = $(this),
                    width = carousel.innerWidth();

                if (width >= 600) {
                    width = width / 4;
                } else if (width >= 350) {
                    width = width / 2;
                }

                carousel.jcarousel('items').css('width', Math.ceil(width) + 'px');
            })
            .jcarousel({
                wrap: 'circular'
            });

        $('.jcarousel-control-prev')
            .jcarouselControl({
                target: '-=1'
            });

        $('.jcarousel-control-next')
            .jcarouselControl({
                target: '+=1'
            });

        $('.jcarousel-pagination')
            .on('jcarouselpagination:active', 'a', function() {
                $(this).addClass('active');
            })
            .on('jcarouselpagination:inactive', 'a', function() {
                $(this).removeClass('active');
            })
            .on('click', function(e) {
                e.preventDefault();
            })
            .jcarouselPagination({
                perPage: 1,
                item: function(page) {
                    return '<a href="#' + page + '">' + page + '</a>';
                }
            });
    });

	// fixed menu 
	$(window).scroll(function(){
	  var sticky = $('.list-info ul'),
	      scroll = $(window).scrollTop();

	  if (scroll >= 200) sticky.addClass('fixed');
	  else sticky.removeClass('fixed');
	});
    
   
    $('#posts').masonry({
          itemSelector : '.item-product'
    });
     $('#masonry-cat3').masonry({
          itemSelector : '.col-masonry-cat3'
    });

    $('#tabsleft').bootstrapWizard({'tabClass': 'up-steps', 'debug': false, onShow: function(tab, navigation, index) {
            console.log('onShow');
        }, onNext: function(tab, navigation, index) {
            console.log('onNext');
        }, onPrevious: function(tab, navigation, index) {
            console.log('onPrevious');
        }, onLast: function(tab, navigation, index) {
            console.log('onLast');
        }, onTabClick: function(tab, navigation, index) {
            console.log('onTabClick');

        }, onTabShow: function(tab, navigation, index) {
            console.log('onTabShow');
            var $total = navigation.find('li').length;
            var $current = index+1;
            var $percent = ($current/$total) * 100;
            $('#tabsleft .progress-bar').css({width:$percent+'%'});

            if($current >= $total) {
                $('#tabsleft').find('.pager .next').hide();
                $('#tabsleft').find('.pager .finish').show();
                $('#tabsleft').find('.pager .finish').removeClass('disabled');
            } else {
                $('#tabsleft').find('.pager .next').show();
                $('#tabsleft').find('.pager .finish').hide();
            }

        }
    });

    $('#tabsleft .finish').click(function() {
        alert('Finished!, Starting over!');
        $('#tabsleft').find("a[href*='tabsleft-tab1']").trigger('click');
    });


    $('#order').bootstrapWizard({'tabClass': 'bwizard-steps'});
    


    $(document).on("scroll", onScroll);

        $('a[href^="#"]').on('click', function (e) {
            e.preventDefault();
            $(document).off("scroll");
                    
            $('a').each(function () {
                $(this).removeClass('active');
            })
            $(this).addClass('active');
                  
            var target = this.hash,
                menu = target;
            $target = $(target);
            $('html, body').stop().animate({
                'scrollTop': $target.offset().top - 100
            }, 1000, 'swing', function () {
                window.location.hash = target;
                $(document).on("scroll", onScroll);
            });
        });

        function onScroll(event){
            var scrollPos = $(document).scrollTop();
            $('.list-info a').each(function () {
                var currLink = $(this);
                var refElement = $(currLink.attr("href"));
                if (refElement.position().top <= scrollPos && refElement.position().top + refElement.height() - 100 > scrollPos) {
                    $('#menu-center ul li a').removeClass("active");
                    currLink.addClass("active");
                }
                else{
                    currLink.removeClass("active");
                }
            });
        }


        $('.btn-click').click(function() {
            $('.sidebar').toggleClass('active');
        });

});
